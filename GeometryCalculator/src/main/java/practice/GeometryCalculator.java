package practice;

public class GeometryCalculator {

    // если значение radius меньше 0, метод должен вернуть -1
    public static double getCircleSquare(double radius) {
        if(radius < 0) return -1;
        return Math.PI * radius * radius;
    }

    // если значение radius меньше 0, метод должен вернуть -1
    public static double getSphereVolume(double radius) {
        if(radius < 0) return -1;
        return (double) 4 / 3  * Math.PI * Math.pow(radius, 3);
    }

    public static boolean isTrianglePossible(double a, double b, double c) {
        return (a < (b + c)) && (b < (a + c)) && (c < (a + b));
    }

    // перед расчетом площади рекомендуется проверить возможен ли такой треугольник
    // методом isTrianglePossible, если невозможен вернуть -1.0
    public static double getTriangleSquare(double a, double b, double c) {
        if(!isTrianglePossible(a,b,c)) return -1.0;
        double halfOfPerimetr = 0.5 * (a + b + c);
        return Math.sqrt(halfOfPerimetr * (halfOfPerimetr-a)*(halfOfPerimetr-b)*(halfOfPerimetr-c));
    }
}
