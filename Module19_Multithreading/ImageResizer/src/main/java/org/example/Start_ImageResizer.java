package org.example;

import java.io.File;

//convert images from srcFolder to small images (with width=300) to dstFolder
public class Start_ImageResizer {
    private static final int newWidth = 300;

    public static void main(String[] args) {

        String srcPath = "data/srcFolder";
        String dstPath = "data/dstFolder";

        long startTimestamp = System.currentTimeMillis();
        int cores = Runtime.getRuntime().availableProcessors();

        // creating array of image files and starting each one in different threads
        File srcDir = new File(srcPath);
        File[] files = srcDir.listFiles();

        // if number of files is not multiple of the number of cores then divide by cores-1
        // and in last thread pass all remaining files
        int arrayLength = (files.length % (double)cores == 0) ? files.length / cores : files.length / (cores - 1);
        for (int i = 0; i < cores; i++) {
            int indexPosition = i * arrayLength;
            if(i == cores - 1){
                arrayLength = files.length - indexPosition;
            }
            File[] filesI = new File[arrayLength];
            System.arraycopy(files, indexPosition, filesI, 0, arrayLength);

            ImageResizer imageResizer = new ImageResizer(filesI, newWidth, dstPath, startTimestamp);
            new Thread(imageResizer).start();
        }
    }
}