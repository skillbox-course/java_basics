package org.example;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageResizer implements Runnable{

    private final File[] files;
    private final int newWidth;
    private final String dstPath;
    private final long startTimestamp;

    public ImageResizer(File[] files, int newWidth, String dstPath, long startTimestamp) {
        this.files = files;
        this.newWidth = newWidth;
        this.dstPath = dstPath;
        this.startTimestamp = startTimestamp;
    }

    @Override
    public void run() {
        try {
            for (File file : files) {
                BufferedImage image = ImageIO.read(file);
                if(image == null){
                    continue;
                }
                int newHeight = (int)Math
                        .round(image.getHeight() / (image.getWidth() / (double)newWidth));
                BufferedImage newImage =
                        new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
                int widthStep = image.getWidth() / newWidth;
                int heightStep = image.getHeight() / newHeight;
                for (int x = 0; x < newWidth; x++){
                    for (int y = 0; y < newHeight; y++){
                        int rgb = image.getRGB(x * widthStep, y * heightStep);
                        newImage.setRGB(x, y, rgb);
                    }
                }
                File newFile = new File(dstPath + "/" + file.getName());
                ImageIO.write(newImage, "jpg", newFile);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("duration: " + (System.currentTimeMillis() - startTimestamp));
    }
}
