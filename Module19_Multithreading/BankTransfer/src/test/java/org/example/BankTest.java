package org.example;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BankTest extends TestCase {
    Account account1 = new Account(500_000, "abc1");
    Account account2 = new Account(400_000, "abc2");
    Account account3 = new Account(600_000, "abc3");
    Account account4 = new Account(300_000, "abc4");
    Account account5 = new Account(200_000, "abc5");
    Account account6 = new Account(700_000, "abc6");
    Bank bank = new Bank(Stream.of(account1, account2, account3, account4, account5, account6)
            .collect(Collectors.toList()));

    public void testTransfer() {
        bank.transfer(account1.getAccNumber(), account2.getAccNumber(), 1000);
        assertEquals(499_000, bank.getBalance("abc1"));
        assertEquals(401_000, bank.getBalance("abc2"));
    }

    public void randomTransfers() {
        for (int i = 0; i < 30; i++) {
            int firstAccNumber = (int) (Math.random() * 6);
            int secondAccNumber = (int) (Math.random() * 6);
            bank.transfer(
                    bank.getAccountById(firstAccNumber).getAccNumber(),
                    bank.getAccountById(secondAccNumber).getAccNumber(),
                    (int) (Math.random() * (51_000 - 5000)) + 5000);
        }
    }

    public void testMultithreadingTransfer() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            threads.add(new Thread(this::randomTransfers));
        }
        threads.forEach(Thread::start);
        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void testGetSumAllAccounts() {
        assertEquals(2_700_000, bank.getSumAllAccounts());
    }

}