package org.example;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class Account implements Comparable<Account> {

    @GuardedBy("this")
    private volatile long money;
    private String accNumber;
    private boolean isBlocked;

    public Account(long money, String accNumber) {
        this.money = money;
        this.accNumber = accNumber;
    }

    public synchronized long getMoney() {
        if (isBlocked){
            System.out.println("account " + accNumber + " is blocked");
            return 0;
        }
        return money;
    }

    public synchronized void setMoney(long money) {
        if (isBlocked){
            System.out.println("account " + accNumber + " is blocked");
        } else {
            this.money = money;
        }
    }

    public String getAccNumber() {
        if (isBlocked){
            System.out.println("account " + accNumber + " is blocked");
        }
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public int compareTo(Account o) {
        return this.getAccNumber().compareToIgnoreCase(o.getAccNumber());
    }
}
