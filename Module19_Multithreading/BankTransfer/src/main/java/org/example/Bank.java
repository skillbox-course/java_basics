package org.example;


import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class Bank {

    // map <accNumber, account>
    private Map<String, Account> accounts;
    private final Random random = new Random();
    private final AtomicLong totalAmountOfMoney = new AtomicLong();

    public Bank(List<Account> accounts) {
        bankInit(accounts);
    }

    private void bankInit(List<Account> accounts) {
        this.accounts = new HashMap<>();
        addListAccounts(accounts);
        accounts.forEach(account -> totalAmountOfMoney.addAndGet(account.getMoney()));
    }

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
            throws InterruptedException {
        System.out.println("========checking===========");
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами. Если сумма транзакции > 50000,
     *  то после совершения транзакции, она отправляется на проверку Службе Безопасности – вызывается
     *  метод isFraud. Если возвращается true, то делается блокировка счетов (как – на ваше
     *  усмотрение)
     */
    public void transfer(String fromAccountNum, String toAccountNum, long amount) {
        long totalBankBalance = getSumAllAccounts();
        if (fromAccountNum.equals(toAccountNum)) {
            return;
        }
        if (accounts.get(fromAccountNum) == null || accounts.get(toAccountNum) == null) {
            System.out.println("there is no such account");
            return;
        }
        if (accounts.get(fromAccountNum).getMoney() <= amount) {
            System.out.println("There is not enough money in the " + fromAccountNum + " account");
            return;
        }
        // checking is operation fraud
        if (amount > 50_000) {
            try {
                if (isFraud(fromAccountNum, toAccountNum, amount)) {
                    accounts.get(fromAccountNum).setBlocked(true);
                    accounts.get(toAccountNum).setBlocked(true);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        // checking the money in the account and doing transfer
        synchronized (accounts.get(
                fromAccountNum.compareToIgnoreCase(toAccountNum) < 0 ? fromAccountNum : toAccountNum)){
            synchronized (accounts.get(
                    fromAccountNum.compareToIgnoreCase(toAccountNum) > 0 ? fromAccountNum : toAccountNum)){
                accounts.get(fromAccountNum).setMoney(this.getBalance(fromAccountNum) - amount);
                accounts.get(toAccountNum).setMoney(this.getBalance(toAccountNum) + amount);
                System.out.println("money transfer");
            }
        }

        if (totalBankBalance != getSumAllAccounts()) {
            throw new RuntimeException("the total amount of all money in the bank has changed");
        }
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public long getBalance(String accountNum) {
        Account account = accounts.get(accountNum);
        return account.getMoney();
    }

    public long getSumAllAccounts() {
        return totalAmountOfMoney.get();
    }

    public void addListAccounts(List<Account> accounts) {
        accounts.forEach(account -> this.accounts.put(account.getAccNumber(), account));
    }

    public Account getAccountById(int i) {
        return new ArrayList<>(accounts.values()).get(i);
    }

}
