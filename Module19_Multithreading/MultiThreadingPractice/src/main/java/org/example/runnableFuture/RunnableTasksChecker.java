package org.example.runnableFuture;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableFuture;

public class RunnableTasksChecker implements Runnable{

    private final List<RunnableFuture<String>> runnableFutureTasksList;

    public RunnableTasksChecker(List<RunnableFuture<String>> runnableFutureTasksList) {
        this.runnableFutureTasksList = runnableFutureTasksList;
    }

    @Override
    public void run() {

        int completedTasks = 0;
        while (completedTasks != 3) {
            for (Iterator<RunnableFuture<String>> tasksIterator = runnableFutureTasksList.iterator();
                 tasksIterator.hasNext();) {
                RunnableFuture<String> task = tasksIterator.next();
                if (task.isDone()){
                    completedTasks++;
                    try {
                        System.out.println(task.get() + " is done");
                        tasksIterator.remove();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (completedTasks == 2) {
                    completedTasks++;
                    task.cancel(true);
                    System.out.println("runnableTask is stopped. It's result is not required!");
                }
            }
        }

    }
}
