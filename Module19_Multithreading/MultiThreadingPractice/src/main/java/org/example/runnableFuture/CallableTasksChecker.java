package org.example.runnableFuture;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableFuture;

public class CallableTasksChecker implements Runnable{

    private final List<RunnableFuture<Integer>> runnableFutureTasksList;

    public CallableTasksChecker(List<RunnableFuture<Integer>> runnableFutureTasksList) {
        this.runnableFutureTasksList = runnableFutureTasksList;
    }

    @Override
    public void run() {

        int completedTasks = 0;
        while (completedTasks != 3) {
            for (Iterator<RunnableFuture<Integer>> tasksIterator = runnableFutureTasksList.iterator();
                 tasksIterator.hasNext();) {
                RunnableFuture<Integer> task = tasksIterator.next();
                if (task.isDone()){
                    completedTasks++;
                    try {
                        System.out.println("callableTask is done by " + task.get() + " milliseconds");
                        tasksIterator.remove();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (completedTasks == 2) {
                    completedTasks++;
                    task.cancel(true);
                    System.out.println("callableTask is stopped. It's result is not required!");
                }
            }
        }

    }
}
