package org.example.runnableFuture;

import java.util.concurrent.Callable;

public class CallableTask implements Callable<Integer> {

    private final Integer timeToCompute;
    private final String taskName;

    public CallableTask(Integer timeToCompute, String taskName) {
        this.timeToCompute = timeToCompute;
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    @Override
    public Integer call() throws Exception {
        System.out.println(getTaskName() + " will take " +
                timeToCompute + " milliseconds");
        Thread.sleep(timeToCompute);
        return timeToCompute;
    }
}
