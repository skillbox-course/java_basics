package org.example.runnableFuture;

public class RunnableTask implements Runnable{

    private final Integer timeToCompute;
    private final String taskName;

    public RunnableTask(Integer timeToCompute, String taskName) {
        this.timeToCompute = timeToCompute;
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    @Override
    public void run() {
        System.out.println(getTaskName() + " will take " +
                timeToCompute + " milliseconds");
        try {
            Thread.sleep(timeToCompute);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
