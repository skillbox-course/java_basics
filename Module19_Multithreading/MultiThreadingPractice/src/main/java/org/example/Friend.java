package org.example;

public class Friend implements Comparable<Friend>{
    private String name;

    public Friend(String name) {
        this.name = name;
    }

    public void throwBallTo(Friend friend) {
        System.out.format("%s threw the ball to %s!%n", this.name, friend.getName());
        synchronized (compareTo(friend) > 0 ? friend : this){
            friend.throwBallTo(this);
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Friend friend) {
        return this.getName().compareTo(friend.getName());
    }
}