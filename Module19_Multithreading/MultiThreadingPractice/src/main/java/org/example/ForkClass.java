package org.example;

import java.util.concurrent.RecursiveTask;

public class ForkClass extends RecursiveTask<Integer> {
    int left, right;
    int[] list;

    public ForkClass(int left, int right, int[] list) {
        this.left = left;
        this.right = right;
        this.list = list;
    }

    @Override
    protected Integer compute() {
        if (right - left < (list.length / 6)) {
            int currentMax = 0;
            for (int i = left; i < right; i++) {
                currentMax = Math.max(currentMax, list[i]);
            }
            return currentMax;
        } else {
            int middle = (left + right) / 2;
            RecursiveTask<Integer> leftTask = new ForkClass(left, middle, list);
            RecursiveTask<Integer> rightTask = new ForkClass(middle + 1, right, list);

            leftTask.fork();
            rightTask.fork();

            return Math.max(leftTask.join(), rightTask.join());
        }
    }
}

























