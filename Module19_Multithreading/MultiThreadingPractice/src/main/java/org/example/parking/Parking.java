package org.example.parking;

public class Parking {
    private int parkingSize = 500;
    private int carCount = 0;

    public synchronized void in(){
        if (carCount == parkingSize){
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        carCount++;
        System.out.println("The car is parked");
        System.out.println("Vacant parking spaces: " + (parkingSize - carCount));
        notify();
    }

    public synchronized void out(){
        if (carCount == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        carCount--;
        System.out.println("The car pulled out of the parking space");
        System.out.println("Free parking spaces: " + (parkingSize - carCount));
        notify();
    }
}
