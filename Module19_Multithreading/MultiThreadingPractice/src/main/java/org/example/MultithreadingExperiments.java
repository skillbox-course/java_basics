package org.example;

import org.example.parking.CarConsumer;
import org.example.parking.Parking;
import org.example.parking.Producer;
import org.example.runnableFuture.CallableTask;
import org.example.runnableFuture.CallableTasksChecker;
import org.example.runnableFuture.RunnableTask;
import org.example.runnableFuture.RunnableTasksChecker;
import org.example.testClasses.Task;
import org.example.testClasses.ValueStorage;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class MultithreadingExperiments {
    private final List<Double> numbers = new ArrayList<>();

    // practice of using atomic variables
    // if perform a non-atomic operations with non-atomic variables
    // from different threads, then the result may not be correct
    public void startThreads(int threadsCount) {
        for (int i = 0; i < threadsCount; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    ValueStorage.incrementValue();
                }
                System.out.println(ValueStorage.getValue());
            }).start();
        }
    }

    // practice of using volatile
    // if read and write non-volatile variable from different threads
    // then result may not be correct
    public void startTask() {
        Task task = new Task();
        new Thread(task).start();
        System.out.println("press enter");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        task.stop();
        System.out.println("Main: " + task.getCounter());
    }

    private synchronized void someHeavyMethod() {
        for (int i = 0; i < 1_000_000; i++) {
            double value = Math.random() / Math.random();
            synchronized (numbers) {
                numbers.add(value);
            }
        }
        System.out.println(numbers.size());
        numbers.clear();
    }

    // running synchronized methods, blocks and classes are thread safety. Threads are queued
    public void startHeavyMethodInMultiThreading() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            threads.add(new Thread(this::someHeavyMethod));
        }
        threads.forEach(Thread::start);
    }

    // wait notify skillbox
    public void startParking() {
        Parking parking = new Parking();
        Producer producer = new Producer(parking);
        CarConsumer carConsumer = new CarConsumer(parking);
        new Thread(producer).start();
        new Thread(carConsumer).start();
    }

    // deadlock skillbox
    public void startVolleyball() {
        Friend vasya = new Friend("Vasya");
        Friend petya = new Friend("Petya");
        new Thread(() -> vasya.throwBallTo(petya)).start();
        new Thread(() -> petya.throwBallTo(vasya)).start();
    }

    // callable
    public void callablePractice() {
        Callable<Double> callable = () -> {
            double sum = 0;
            for (int i = 0; i < 1000; i++) {
                sum += Math.random();
            }
            if (sum < 600) {
                throw new IllegalArgumentException("sum exception");
            }
            return sum / 1000;
        };
        FutureTask<Double> futureTask = new FutureTask<>(callable);
        new Thread(futureTask).start();

        try {
            System.out.println(futureTask.get());
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            e.getMessage();
        }
    }

    public void executorPractice() {

//        Executor executor = Executors.newSingleThreadExecutor();
//        executor.execute(() -> {
//            for (int i = 0; i < 100; i++){
//                System.out.println(Math.random());
//            }
//        });

        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<Double> future = service.submit(() -> {
            double sum = 0;
            for (int i = 0; i < 100_000; i++) {
                sum += Math.random();
            }
            return sum;
        });
        try {
            System.out.println(future.get());
            service.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void threadGroupPractice() {
        Thread currentThread = Thread.currentThread();
        ThreadGroup threadGroup = currentThread.getThreadGroup();
        System.out.println("Thread: " + currentThread.getName());
        System.out.println("Thread Group: " + threadGroup.getName());
        System.out.println("Parent Group: " + threadGroup.getParent().getName());
        currentThread.setUncaughtExceptionHandler((t, e) -> System.out.println("exception: " + e.getMessage()));
        System.out.println(2 / 0);
    }

    public void synchronizedPractice() {
        Object lock = new Object();

        Runnable task = () -> {
            synchronized (lock) {
                System.out.println(Thread.currentThread().getName());
            }
        };
        new Thread(task).start();

        synchronized (lock) {
            for (int i = 0; i < 4; i++) {
                try {
                    Thread.currentThread().sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(" " + i);
            }
            System.out.println(" ...");
        }
    }

    public void waitPractice() {
        System.out.println("=================");
        Object lock = new Object();

        Runnable task = () -> {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println(Thread.currentThread().getName());
        };
        new Thread(task).start();

        try {
            Thread.currentThread().sleep(2000);
            System.out.println(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        synchronized (lock) {
            lock.notify();
        }
    }

    public void semaphorePractice() {
        Semaphore semaphore = new Semaphore(1);
        try {
            semaphore.acquire();
            System.out.println("permission has been received");
            semaphore.release();
            semaphore.acquire();
            System.out.println("permission has been received");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void lockSupportPractice() {
        Runnable task = () -> {
            System.err.println("will be parked");
            LockSupport.park();
            System.err.println("unparked");
        };
        Thread thread = new Thread(task);
        thread.start();

        System.out.println(Thread.currentThread().getName());
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.err.println("Thread state " + thread.getState());
        LockSupport.unpark(thread);
    }

    public void completableFuturePractice() {
        //===== CompletableFuture =====
        AtomicLong dateMilliseconds = new AtomicLong(0);
        Runnable task = () -> dateMilliseconds.set(new Date().getTime());
        Function<Long, Date> dateConverter = Date::new;
        Consumer<Date> printer = date -> {
            System.out.println(date);
            System.out.flush();
        };
        CompletableFuture.runAsync(task)
                .thenApply((v) -> dateMilliseconds.get())
                .thenApply(dateConverter)
                .thenAccept(printer);
        //===== CompletableFuture combine =====
        Supplier<String> messageSupplier = () -> {
            try {
                Thread.sleep(1000);
                return "message";
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        // по умолчанию это демон потоки, поэтому в конце .get()
        CompletableFuture<String> reader = CompletableFuture.supplyAsync(messageSupplier);
        try {
            CompletableFuture.completedFuture("!!")
                    .thenCombine(reader, (s, s2) -> s + s2)
                    .thenAccept(System.out::println)
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        //===== CompletableFuture Compose =====
        // executed in the same thread. Compose return CompletableFuture
        CompletableFuture.completedFuture(32L)
                .thenCompose(aLong -> CompletableFuture.completedFuture(aLong + 2))
                .thenAccept(System.out::println);
        //===== CompletableFuture exceptionally =====
        // exception handling
        CompletableFuture.completedFuture(2L)
                .thenAccept(a -> {
                    throw new IllegalArgumentException("error");
                })
                .thenApply((a) -> 3L)
                .exceptionally(ex -> 0L)
                .thenAccept(System.out::println);
    }

    public void executorServicePractice() {
        //===== ExecutorService =====
        Callable<String> callableTask = () -> Thread.currentThread().getName();
        ExecutorService service = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 5; i++) {
            Future result = service.submit(callableTask);
            try {
                System.out.println(result.get());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
        service.shutdown();

        // ===== ThreadPoolExecutor =====
        int threadBound = 2;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, threadBound,
                0L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(2));
        Callable<String> task = () -> {
            Thread.sleep(1000);
            return Thread.currentThread().getName();
        };
        for (int i = 0; i < threadBound + 1; i++) {
            threadPoolExecutor.submit(task);
        }
        threadPoolExecutor.shutdown();

        threadPoolExecutor = new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.SECONDS, new SynchronousQueue());
        task = () -> Thread.currentThread().getName();
        threadPoolExecutor.setRejectedExecutionHandler((runnable, executor) -> System.out.println("Rejected"));
        for (int i = 0; i < 5; i++) {
            threadPoolExecutor.submit(task);
        }
        threadPoolExecutor.shutdown();

        //===== ScheduledExecutorService =====
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
        Runnable task2 = () -> System.out.println(Thread.currentThread().getName());
        scheduledExecutorService.scheduleAtFixedRate(task2, 1, 2, TimeUnit.SECONDS);
        scheduledExecutorService.shutdown();

        //===== WorkStealingPool =====
        Object lock = new Object();
        ExecutorService executorService = Executors.newWorkStealingPool();
        Callable<String> task3 = () -> {
            System.out.println(Thread.currentThread().getName());
            lock.wait(1000);
            System.out.println("Finished");
            return "result";
        };
        for (int i = 0; i < 5; i++) {
            executorService.submit(task3);
        }
        executorService.shutdown();
    }

    public void threadBarrierPractice() {
        //===== Semaphore =====
//        Semaphore semaphore = new Semaphore(0);
//        Runnable taskSemaphore = () -> {
//            try {
//                semaphore.acquire();
//                System.out.println("finish");
//                semaphore.release();
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        };
//        new Thread(taskSemaphore).start();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//        semaphore.release(1);

        //===== CountDownLatch =====
//        CountDownLatch countDownLatch = new CountDownLatch(4);
//        Runnable taskCountDownLatch = () -> {
//            try {
//                countDownLatch.countDown();
//                System.out.println("CountDown: " + countDownLatch.getCount() + " " + Thread.currentThread().getName());
//                countDownLatch.await();
//                System.out.println("finish " + Thread.currentThread().getName());
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        };
//        for (int i =0; i < 4; i++){
//            new Thread(taskCountDownLatch).start();
//        }

        //===== CyclicBarrier =====
//        Runnable action = () -> System.out.println("start");
//        CyclicBarrier barrier = new CyclicBarrier(3, action);
//        Runnable task = () -> {
//            try {
//                barrier.await();
//                System.out.println(Thread.currentThread().getName() + " finish");
//            } catch (InterruptedException | BrokenBarrierException e) {
//                throw new RuntimeException(e);
//            }
//        };
//        System.out.println("Limit: " + barrier.getParties());
//        for (int i = 0; i < 3; i++){
//            new Thread(task).start();
//        }

        //===== Exchanger =====
//        Exchanger<String> exchanger = new Exchanger<>();
//        Runnable taskExchanger = () -> {
//            try {
//                String withThreadName = exchanger.exchange(Thread.currentThread().getName());
//                System.out.println(Thread.currentThread().getName() + " обменялся с " + withThreadName);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        };
//        new Thread(taskExchanger).start();
//        new Thread(taskExchanger).start();

        //===== Phaser =====
        Phaser phaser = new Phaser();
        System.out.println("Phasercount is " + phaser.getPhase());
        Runnable task = () -> {
            phaser.register();
            System.out.println(Thread.currentThread().getName() + " arrived");
            phaser.arriveAndAwaitAdvance();
            System.out.println(Thread.currentThread().getName() +
                    " after passing barrier");
        };
        for (int i = 0; i < 4; i++) {
            new Thread(task).start();
        }
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        phaser.arriveAndDeregister();
        System.out.println("Phasecount is " + phaser.getPhase());
    }

    public void threadInterruptPractice() {
        Thread thread = new Thread(() -> {
            double sum = 0;
            for (; ; ) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println(Thread.currentThread().getName() +
                            " is interrupted%n" + sum);
                }
                sum += Math.random();
            }
        });
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        thread.interrupt();
    }

    public void threadPoolExecutors() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
        executor.setMaximumPoolSize(10);
    }

    public void forkJoinPoolPractice(){
        int max = 0;
        int[] list = new int[100_000];
        for (int i = 0; i < 100_000; i++) {
            list[i] = (int) (Math.random() * 1_000_000);
            max = Math.max(max, list[i]);
        }

        System.out.println(findMax(list));
        System.out.println(max);
    }

    private int findMax(int[] list) {
        RecursiveTask<Integer> recursiveTask = new ForkClass(0, list.length, list);
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        return forkJoinPool.invoke(recursiveTask);
    }

    // // из 3-х callableTasks и 3-х runnableTasks должны выполниться 2(is done) и отмениться 3-я задача(is stopped)
    public void runnableFuturePractice() {
        RunnableFuture<Integer> task1 = new FutureTask<>(
                new CallableTask(8000, "callableTask1"));
        RunnableFuture<Integer> task2 = new FutureTask<>(
                new CallableTask(3000, "callableTask2"));
        RunnableFuture<Integer> task3 = new FutureTask<>(
                new CallableTask(14000, "callableTask3"));
        RunnableFuture<String> task4 = new FutureTask<>(
                new RunnableTask(5000, "runnableTask4"), "runnableTask4");
        RunnableFuture<String> task5 = new FutureTask<>(
                new RunnableTask(2000, "runnableTask5"), "runnableTask5");
        RunnableFuture<String> task6 = new FutureTask<>(
                new RunnableTask(9000, "runnableTask6"), "runnableTask6");

        Stack<RunnableFuture<Integer>> tasksList1 = new Stack<>();
        tasksList1.add(task1);
        tasksList1.add(task2);
        tasksList1.add(task3);
        List<RunnableFuture<String>> taskList2 = new ArrayList<>();
        taskList2.add(task4);
        taskList2.add(task5);
        taskList2.add(task6);

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        tasksList1.forEach(task -> executorService.execute(task));
        CallableTasksChecker callableTasksChecker = new CallableTasksChecker(tasksList1);
        executorService.execute(callableTasksChecker);

        taskList2.forEach(task -> executorService.execute(task));
        RunnableTasksChecker runnableTasksChecker = new RunnableTasksChecker(taskList2);
        executorService.execute(runnableTasksChecker);

        executorService.shutdown();
    }
}























