package org.example;

public class Start_Practice {
    public static void main(String[] args){

        MultithreadingExperiments experiments = new MultithreadingExperiments();

//        experiments.startThreads(4);
//        experiments.startTask();
//        experiments.startHeavyMethodInMultiThreading();
//        experiments.startingParking();
//        experiments.startingVolleyball();
//        experiments.callablePractice();
//        experiments.executorPractice();
//        experiments.threadGroupPractice();
//        experiments.synchronizedPractice();
//        experiments.waitPractice();
//        experiments.semaphorePractice();
//        experiments.lockSupportPractice();
//        experiments.completableFuturePractice();
//        experiments.executorServicePractice();
//        experiments.threadBarrierPractice();
//        experiments.forkJoinPoolPractice();
        experiments.runnableFuturePractice();
    }
}