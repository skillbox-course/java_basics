package org.example;

import javax.naming.InsufficientResourcesException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class Transfer implements Callable<Boolean> {
    private final Account account1;
    private final Account account2;
    private final int amount;

    public Transfer(Account account1, Account account2, int amount) {
        this.account1 = account1;
        this.account2 = account2;
        this.amount = amount;
    }

    @Override
    public Boolean call() throws Exception {
        if (account1.getLock().tryLock(2, TimeUnit.SECONDS)){
            try {
                if (account1.getBalance() < amount){
                    throw new InsufficientResourcesException();
                }
                if (account1.getLock().tryLock(2, TimeUnit.SECONDS)){
                    try {
                        account1.withdraw(amount);
                        account2.deposit(amount);
                        return true;
                    } finally {
                        account2.getLock().unlock();
                    }
                } else {
                    return false;
                }
            } finally {
                account1.getLock().unlock();
            }
        } else {
            return false;
        }
    }
}
