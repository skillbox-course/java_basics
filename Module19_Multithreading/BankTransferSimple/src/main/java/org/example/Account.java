package org.example;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Comparable<Account>{
    private final String number;
    private int balance;
    private final Lock lock;

    public Account(String number, int balance) {
        this.number = number;
        this.balance = balance;
        lock = new ReentrantLock();
    }

    public void withdraw(int amount){
        balance -= amount;
    }

    public void deposit(int amount){
        balance += amount;
    }

    public String getNumber() {
        return number;
    }

    public int getBalance() {
        return balance;
    }

    public Lock getLock() {
        return lock;
    }

    @Override
    public int compareTo(Account o) {
        return this.getNumber().compareTo(o.getNumber());
    }
}
