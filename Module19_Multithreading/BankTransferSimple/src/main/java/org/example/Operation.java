package org.example;

import lombok.extern.slf4j.Slf4j;

import javax.naming.InsufficientResourcesException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Operation {

    public void transfer(Account account1, Account account2, int amount)
            throws InsufficientResourcesException, InterruptedException {

        if (account1.getBalance() < amount) {
            throw new InsufficientResourcesException();
        }

        log.warn("trying to lock account1 " + account1.hashCode());
        synchronized (account1.compareTo(account2) < 0 ? account1 : account2) {
            log.warn("lock account1 " + account1.hashCode());
            Thread.sleep(1000);
            log.warn("trying to lock account2 " + account2.hashCode());
            synchronized (account1.compareTo(account2) > 0 ? account1 : account2) {
                log.warn("lock account2 " + account2.hashCode());
                account1.withdraw(amount);
                account2.deposit(amount);
                log.info("transfer successful!");
            }
        }
    }

    public void transferConcurrencyWithLock(Account account1, Account account2, int amount)
            throws InsufficientResourcesException, InterruptedException {

        if (account1.getBalance() < amount) {
            throw new InsufficientResourcesException();
        }

        if (account1.getLock().tryLock(2, TimeUnit.SECONDS)) {
            try {
                if (account2.getLock().tryLock(2, TimeUnit.SECONDS)) {
                    try {
                        account1.withdraw(amount);
                        account2.deposit(amount);
                        log.info("transfer successful!");
                    } finally {
                        account2.getLock().unlock();
                    }
                } else {
                    throw new InsufficientResourcesException();
                }
            } finally {
                account1.getLock().unlock();
            }
        } else {
            throw new InsufficientResourcesException();
        }
    }
}
