package org.example;

import javax.naming.InsufficientResourcesException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InsufficientResourcesException, InterruptedException {
        Operation operation = new Operation();
        final Account account1 = new Account("A001", 1000);
        final Account account2 = new Account("A002", 2000);

//        new Thread(() -> {
//            try {
//                operation.transfer(account1, account2, 100);
//            } catch (InsufficientResourcesException | InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }).start();
//
//        new Thread(() -> {
//            try {
//                operation.transfer(account2, account1, 100);
//            } catch (InsufficientResourcesException | InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }).start();

        ExecutorService service = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++){
            service.submit(new Transfer(account1, account2, new Random().nextInt(600)));
        }
        service.shutdown();
        service.awaitTermination(10, TimeUnit.SECONDS);
        System.out.println(account1.getBalance());
        System.out.println(account2.getBalance());
    }
}