package org.example;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;

@Component
public class BeanOne {

    private final BeanTwo beanTwo;

    public BeanOne(BeanTwo beanTwo) {
        this.beanTwo = beanTwo;
    }

    public void sayHello() {
        System.out.println("bean one: hello");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("calling postConstruct method");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("calling preDestroy method");
    }
}
