package org.example.config;


import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter@Setter
@Component
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    private String port;
    private String name;
    private int maxConnections;
    private boolean enabledFeature;
    private String commentMaxLength;

    @PostConstruct
    public void printConfig() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "AppConfig{" +
                "port='" + port + '\'' +
                ", name='" + name + '\'' +
                ", maxConnections=" + maxConnections +
                ", enabledFeature=" + enabledFeature +
                ", commentMaxLength='" + commentMaxLength + '\'' +
                '}';
    }
}
