package org.example;

import org.springframework.stereotype.Component;

@Component
public class BeanTwo {

    public void sayHello() {
        System.out.println("bean two: hello");
    }
}
