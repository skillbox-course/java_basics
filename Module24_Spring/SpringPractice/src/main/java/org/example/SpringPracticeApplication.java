package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringPracticeApplication {
    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(SpringPracticeApplication.class, args);
        SpringPractice practice = new SpringPractice();
        practice.beanPractice(context);

    }
}


