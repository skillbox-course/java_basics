package org.example;

import org.springframework.context.ApplicationContext;

public class SpringPractice {

    public void beanPractice(ApplicationContext context) {
        String[] beanNames = context.getBeanDefinitionNames();
        System.out.println("count of beans: " + beanNames.length);
//        Arrays.stream(beanNames).forEach(System.out::println);
        BeanOne beanOne = context.getBean(BeanOne.class);
        BeanTwo beanTwo = context.getBean(BeanTwo.class);
        beanOne.sayHello();
        beanTwo.sayHello();
    }
}
