package org.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class DefaultController {

    @ResponseBody
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String defaultRequest() {
        return "Hello World!";
    }


    @ResponseBody
    @RequestMapping(path = "/params", method = RequestMethod.GET)
    public String paramRequest(@RequestParam String param) {
        return "request param - " + param;
    }

    @ResponseBody
    @RequestMapping(path = "/pathparam/{param}", method = RequestMethod.GET)
    public String pathParamRequest(@PathVariable String param) {
        return "path param - " + param;
    }


    @ResponseBody
    @RequestMapping(path = "/postrequest", method = RequestMethod.POST)
    public String postRequest(@RequestParam String param) {
        return "post param: " + param;
    }

}