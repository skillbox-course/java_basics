package org.example.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.TreeMap;

@RestController
public class TodoController {
    private final TreeMap<Integer, String> todo;

    public TodoController() {
        this.todo = new TreeMap<>();
        todo.put(1, "wake up");
        todo.put(2, "wash up");
    }

    @GetMapping(path = "/todo")
    public TreeMap<Integer, String> getTodo() {
        return todo;
    }

    @PostMapping(path = "/todo")
    public ResponseEntity addTask(@RequestParam String task) {
        if (task.isEmpty() || task == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        todo.put(todo.lastKey() + 1, task);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(path = "/todo/{id}")
    public ResponseEntity changeTask(@PathVariable int id, @RequestParam String task) {
        todo.put(id, task);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(path = "/todo/{id}")
    public ResponseEntity deleteTask(@PathVariable int id) {
        todo.remove(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}




















