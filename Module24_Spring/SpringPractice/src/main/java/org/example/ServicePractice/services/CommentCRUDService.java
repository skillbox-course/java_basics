package org.example.ServicePractice.services;

import org.example.ServicePractice.dto.CommentDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.TreeMap;

@Service
public class CommentCRUDService implements CRUDService<CommentDTO> {

    @Value("${comment.length.max}")
    private Integer maxLength;
    private final TreeMap<Integer, CommentDTO> storage = new TreeMap<>();

    @Override
    public CommentDTO readById(Integer id) {
        System.out.println("get by id " + id);
        return storage.get(id);
    }

    @Override
    public Collection<CommentDTO> readAll() {
        System.out.println("get all");
        return storage.values();
    }

    @Override
    public void create(CommentDTO item) {
        System.out.println("create");
        int id = storage.isEmpty() ? 0 : storage.lastKey() + 1;
        item.setId(id);
        if (item.getText().length() > maxLength) {
            throw new RuntimeException("comment is too long: " + item.getText().length() +
                    " comment max length: " + maxLength);
        }
        storage.put(id, item);
    }

    @Override
    public void update(Integer id, CommentDTO item) {
        System.out.println("update " + id);
        if (!storage.containsKey(id)) {
            return;
        }
        item.setId(id);
        if (item.getText().length() > maxLength) {
            throw new RuntimeException("comment is too long: " + item.getText().length() +
                    "comment max length: " + maxLength);
        }
        storage.put(id, item);
    }

    @Override
    public void delete(Integer id) {
        System.out.println("delete " + id);
        storage.remove(id);
    }
}
