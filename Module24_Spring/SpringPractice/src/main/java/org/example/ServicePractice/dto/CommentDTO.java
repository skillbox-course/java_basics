package org.example.ServicePractice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter@Getter
// data transfer object
public class CommentDTO {
    private Integer id;
    private String text;
    private String author;
}
