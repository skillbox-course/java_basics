package org.example.ServicePractice.controllers;

import org.example.ServicePractice.dto.CommentDTO;
import org.example.ServicePractice.services.CommentCRUDService;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/comment")
public class CommentController {
    private final CommentCRUDService commentCRUDService;

    public CommentController(CommentCRUDService commentCRUDService) {
        this.commentCRUDService = commentCRUDService;
    }

    @GetMapping("/{id}")
    public CommentDTO getCommentById(@PathVariable Integer id) {
        return commentCRUDService.readById(id);
    }

    @GetMapping
    public Collection<CommentDTO> getAllComments() {
        return commentCRUDService.readAll();
    }

    @PostMapping
    public void createComment(@RequestBody CommentDTO comment) {
        commentCRUDService.create(comment);
    }

    @PutMapping("/{id}")
    public void updateComment(@PathVariable Integer id, @RequestBody CommentDTO comment) {
        commentCRUDService.update(id, comment);
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable Integer id){
        commentCRUDService.delete(id);
    }
}
























