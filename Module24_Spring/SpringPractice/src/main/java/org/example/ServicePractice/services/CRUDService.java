package org.example.ServicePractice.services;

import java.util.Collection;

public interface CRUDService<T> {

    T readById(Integer id);

    Collection<T> readAll();

    void create(T item);

    void update(Integer id, T item);

    void delete(Integer id);
}
