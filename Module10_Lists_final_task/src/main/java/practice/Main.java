package practice;

import java.util.Scanner;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        // TODO: написать консольное приложение для работы со списком дел todoList

        while(true) {
            //initialization of required variables
            System.out.println("\nВведите команду: \nдля выхода наберите EXIT");
            Scanner scanner = new Scanner(System.in);
            String  todo = scanner.nextLine();
            if (todo.equalsIgnoreCase("EXIT")) {
                break;
            }
            String task = "";
            String[] todoWords = todo.split("\s");

            //checking first word - command
            switch (todoWords[0].toUpperCase()) {
                case "ADD":
                    //checking whether the index is entered
                    if (todoWords[1].matches("\\d+")) {
                        for (int i = 2; i < todoWords.length; i++){
                            task = task.concat(todoWords[i]).concat(" ");
                        }
                        todoList.add(Integer.parseInt(todoWords[1]), task.trim());
                    } else {
                        for (int i = 1; i < todoWords.length; i++){
                            task = task.concat(todoWords[i]).concat(" ");
                        }
                        todoList.add(task.trim());
                    }
                    break;
                case "LIST":
                    System.out.println(todoList.toString());
                    break;
                case "EDIT":
                    //checking whether the index is entered
                    if (todoWords[1].matches("\\d+")) {
                        for (int i = 2; i < todoWords.length; i++){
                            task = task.concat(todoWords[i]).concat(" ");
                        }
                       todoList.edit(Integer.parseInt(todoWords[1]), task.trim());
                    }
                    break;
                case "DELETE":
                    todoList.delete(Integer.parseInt(todoWords[1]));
                    break;
            }
        }
    }
}
