package practice;
import java.util.ArrayList;

public class TodoList {
    ArrayList<String> todoTasks = new ArrayList<>();

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка
        todoTasks.add(todo);
        System.out.println("Добавлено дело \"" + todo + "\"");
    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
        if (index >= 0 && index < todoTasks.size()){
            todoTasks.add(index,todo);
            System.out.println("Добавлено дело \"" + todo + "\"");
        } else {
            todoTasks.add(todo);
        }
    }

    public void edit(int index, String todo) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
        if (index >= 0 && index < todoTasks.size()) {
            System.out.print("Дело \"" + todoTasks.get(index));
            todoTasks.set(index, todo);
            System.out.println("\" заменено на \"" + todoTasks.get(index) + "\"");
        }
    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
        if (index >= 0 && index < todoTasks.size()) {
            System.out.println("Дело \"" + todoTasks.get(index) + "\" удалено");
            todoTasks.remove(index);
        } else {
            System.out.println("Дело с таким номером не существует");
        }
    }

    public ArrayList<String> getTodos() {
        // TODO: вернуть список дел
        return todoTasks;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < todoTasks.size(); i++){
            result.append(i).append(" - ").append(todoTasks.get(i)).append("\n");
        }
        return result.toString().trim();
    }
}