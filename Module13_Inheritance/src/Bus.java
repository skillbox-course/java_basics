import java.util.Objects;

public class Bus {
    private double tankFullnessRate;        //rate of tank fullness, 0-1 (уровень заполнености бака)
    //rate (0-1) per 1km  (уровень потребления - расход топлива относительно бака!)
    //0,001 - уровень бака который нужен на 1 км! 0,001 бака на 1 км, полный бак на 1000км
    protected double consumptionRate;
    private static int count;               //total count of bus

    public Bus(double consumptionRate) {
        this.consumptionRate = consumptionRate;
        count++;
    }

    public boolean run(int distance) {
        if (powerReserve() < distance) {
            return false;
        }
        tankFullnessRate -= distance * consumptionRate;
        return true;
    }

    //tank rate - передаются в метод не литры, а объем бака на который заправляется автобус
    public final void refuel(double tankRate) {
        double total = tankFullnessRate + tankRate;
        tankFullnessRate = total > 1 ? 1 : total;
    }

    //
    protected int powerReserve() {
        return (int) (tankFullnessRate / consumptionRate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bus bus = (Bus) o;
        return Double.compare(tankFullnessRate, bus.tankFullnessRate) == 0 && Double.compare(consumptionRate, bus.consumptionRate) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tankFullnessRate, consumptionRate);
    }

    public static int getCount() {
        return count;
    }

    public double getTankFullnessRate() {
        return tankFullnessRate;
    }

    public double getConsumptionRate() {
        return consumptionRate;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "tankFullnessRate=" + tankFullnessRate +
                ", consumptionRate=" + consumptionRate +
                '}';
    }
}
