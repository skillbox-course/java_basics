public class StartModule13 {
    public static void main(String[] args) {

        Bus bus1 = new Bus(0.001);
        Bus bus2 = new Bus(0.001);
        Bus electricBus = new ElectricBus(0.001, 0.1);
        ElectricBus electricBus2 = new ElectricBus(0.001, 0.1);
        ElectricBus electricBus3 = new ElectricBus(0.001, 0.1);
        System.out.println("Total bus count: " + Bus.getCount());
        System.out.println("Electric bus count: " + ElectricBus.getCount());

        electricBus.refuel(1);
        System.out.println("Резерв: " + electricBus.powerReserve());
        System.out.println("Едем 50км: " + electricBus.run(50));
        System.out.println("Резерв: " + electricBus.powerReserve());
        System.out.println("Едем 900км: " + electricBus.run(900));
        System.out.println("Резерв: " + electricBus.powerReserve());
        System.out.println("Едем 100км: " + electricBus.run(100));
        System.out.println("Резерв: " + electricBus.powerReserve());
    }
}
