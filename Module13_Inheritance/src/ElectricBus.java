public class ElectricBus extends Bus {
    private final double minimalTankFullnessRate;       //minimal rate below which the battery can't be discharged
    private static int count;                           //count of electric bus

    public ElectricBus(double consumptionRate, double minimalTankFullnessRate) {
        super(consumptionRate);
        this.minimalTankFullnessRate = minimalTankFullnessRate;
        count++;
    }

    @Override
    public int powerReserve() {
        //actual available charge level. Full battery charge - the minimum charge level
        double remainingRate = getTankFullnessRate() - minimalTankFullnessRate;
        if (remainingRate <= 0) {
            return 0;
        }
        return (int) (remainingRate / getConsumptionRate());
    }

    public static int getCount() {
        return count;
    }
}
