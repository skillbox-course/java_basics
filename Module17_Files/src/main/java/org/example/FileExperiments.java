package org.example;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class FileExperiments {
    
    public String readFileUsingFileInputStream(String path) {
        log.info("\nread file using FileInputStream");
        StringBuilder result = new StringBuilder();
        try (FileInputStream inputStream = new FileInputStream(path)) {
            for(;;){
                int code = inputStream.read();
                if (code < 0){
                    break;
                }
                char ch = (char) code;
                result.append(ch);
            }
        } catch (IOException ex){
            ex.getMessage();
        }
        return result.toString();
    }

    public String readFileUsingBufferedReader(String path) {
        log.info("\nread file using BufferedReader");
        StringBuilder result = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            for (;;){
                String line = bufferedReader.readLine();
                if (line == null){
                    break;
                }
                result.append(line).append("\n");
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result.toString();
    }

    public String readFileUsingFilesClass(String path){
        log.info("\nread file using Files class");
        StringBuilder result = new StringBuilder();
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            lines.forEach(line -> result.append(line).append("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result.toString();
    }

    public void writeInFileUsingPrintWriter(String path){
        try {
            PrintWriter writer = new PrintWriter(path);
            for (int i = 10; i < 100; i++){
                writer.write((i % 10) == 9 ? (i + "\n") : (i + " "));
            }
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeInFileUsingFilesClass(String path){
        List<String> result = new ArrayList<>();
        try {
            for (int i = 10; i < 100; i++){
                result.add(String.valueOf(i));
            }
            Files.write(Paths.get(path), result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public long calculateFileSize(File file){
        if (file.isFile()){
            return file.length();
        }
        long result = 0;
        File[] files = file.listFiles();
        for (File elem : Objects.requireNonNull(files)){
            result += calculateFileSize(elem);
        }
        return result;
    }

    public String getHumanReadableSize(long size){
        float result = size;
        DecimalFormat df = new DecimalFormat("0.00");
        String unitOfMeasurement = "b";
        for (int i = 0; result > 1024; i++){
            result = result / 1024;
            switch (i){
                case 0 -> unitOfMeasurement = "kb";
                case 1 -> unitOfMeasurement = "Mb";
                case 2 -> unitOfMeasurement = "Gb";
            }
        }
        return df.format(result) + unitOfMeasurement;
    }

    // TODO output file or directory size in console by path, that user input in console
    public void getFileSizeByPath(){
        log.info("\nenter directory path:");
        System.out.println("\nfor example Module17_Files/src/main/resources");
        String path = new Scanner(System.in).nextLine();
        long fileSize = calculateFileSize(new File(path));
        System.out.println(getHumanReadableSize(fileSize));
    }

    // TODO parse movementList.csv table.
    //  Output must be HashMap grouped by paymentType(extract from column “Описание операции”). Just expense
    //  Key = paymentType
    //  Value = total expense by paymentType
    public void tableParser(){
        log.info("\npayment type and total expense");
        // тип платежа из фрагмента строки с описанием операции
        String regex = "[^a-zA-Z0-9]([a-zA-Z0-9\s]+)[0-9]{2}\\.[0-9]{2}\\.[0-9]{2}\s[0-9]{2}\\.[0-9]{2}\\.[0-9]{2}";
        String path = "Module17_Files/src/main/resources/movementList.csv";
        Pattern pattern = Pattern.compile(regex);
        Map<String, Double> resultMap = new HashMap<>();
        // деление таблицы на строки. Каждая строка разбивается на поля(столбцы).
        // берутся записи, содержащие только расходы восьмой столбец не нулевой.
        // группируется общая сумма расходов(value) по одному типу платежей(key)
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for (String line : lines){
                String[] fragments = line.split(",");
                Matcher matcher = pattern.matcher(fragments[5]);
                if (matcher.find() && !fragments[7].equals("0")){
                    Double totalExpense =  resultMap.containsKey(matcher.group(1)) ?
                            resultMap.get(matcher.group(1)) + Double.parseDouble(fragments[7])
                            : Double.parseDouble(fragments[7]);
                    resultMap.put(matcher.group(1), totalExpense);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // output to console
        for (String paymentType : resultMap.keySet()){
            System.out.println(paymentType + resultMap.get(paymentType));
        }
    }
}
