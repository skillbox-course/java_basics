package org.example;

import org.example.JSON.JsonExperiments;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class StartModule17 {
    public static void main(String[] args) {
        File file = new File("Module17_Files/src/main/resources/test.txt");
        System.out.println("file size in bytes " + file.length());
        System.out.println("last file modified in milliseconds " + file.lastModified());

        File dir = new File("Module17_Files/src/main/resources");
        System.out.println("is directory " + dir.isDirectory());
        Arrays.stream(Objects.requireNonNull(dir.listFiles()))
                .forEach(fileItem -> System.out.println(fileItem.getAbsoluteFile()));

        FileExperiments fileExperiments = new FileExperiments();
        System.out.println(
                fileExperiments.readFileUsingFileInputStream("Module17_Files/src/main/resources/test.txt"));
        System.out.println(
                fileExperiments.readFileUsingBufferedReader("Module17_Files/src/main/resources/test.txt"));
        System.out.println(
                fileExperiments.readFileUsingFilesClass("Module17_Files/src/main/resources/test.txt"));
        fileExperiments.writeInFileUsingPrintWriter("Module17_Files/src/main/resources/testWrite1.txt");
        fileExperiments.writeInFileUsingFilesClass("Module17_Files/src/main/resources/testWrite2.txt");

        fileExperiments.tableParser();

        HTMLParser htmlParser = new HTMLParser();
        htmlParser.parseHTMLDoc();
//        htmlParser.downloadImagesFromWeb();

        JsonExperiments jsonExperiments = new JsonExperiments();
        jsonExperiments.parseJsonToJavaObject();
        jsonExperiments.readObjectFromJson();
        jsonExperiments.writeObjectFromJson();

//        fileExperiments.getFileSizeByPath();
    }
}