package org.example.JSON;

import lombok.Data;

@Data
public class Car {
    private String licensePlate;
}
