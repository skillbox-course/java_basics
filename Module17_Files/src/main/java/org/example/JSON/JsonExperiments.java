package org.example.JSON;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class JsonExperiments {
    ObjectMapper objectMapper = new ObjectMapper();
    Person person = new Person();

    // extract JsonNode jsonData from filePath.
    // extract JsonNode stations and lines from jsonData.
    // using ObjectNode remove param "color" and adding param "stationsCount"
    // using ObjectMapper write to File modified lines
    public void parseJsonToJavaObject() {
        String jsonFileAsString;
        JsonNode jsonData;
        try {
            jsonFileAsString =
                    Files.readString(Paths.get("Module17_Files/src/main/resources/json/map.json"));
            jsonData = objectMapper.readTree(jsonFileAsString);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        JsonNode stations = jsonData.get("stations");
        JsonNode lines = jsonData.get("lines");
        for (JsonNode line : lines){
            ObjectNode lineNode = (ObjectNode) line;
            lineNode.remove("color");
            String lineNumber = line.get("number").asText();
            JsonNode stationsList = stations.get(lineNumber);
            int stationsCount = stationsList.size();
            lineNode.put("stationsCount", stationsCount);
        }
        File outputFile = new File("Module17_Files/src/main/resources/json/output.json");
        try {
            objectMapper.writeValue(outputFile, lines);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void readObjectFromJson() {
        log.info("parse person.json file to java object person");
        String jsonFileAsString;
        try {
            jsonFileAsString = Files.readString(Paths.get("Module17_Files/src/main/resources/json/person.json"));
            objectMapper.registerModule(new JavaTimeModule());
            person = objectMapper.readValue(jsonFileAsString, Person.class);
            System.out.println(person);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeObjectFromJson() {
        person.setChildren(List.of("Olga", "Petr"));
        Car car = new Car();
        car.setLicensePlate("A111BB777");
        person.setCar(car);
        try {
            String stringJsonPerson = objectMapper.writeValueAsString(person);
            FileWriter fileWriter =
                    new FileWriter("Module17_Files/src/main/resources/json/personModified.json");
            fileWriter.write(stringJsonPerson);
            fileWriter.close();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
