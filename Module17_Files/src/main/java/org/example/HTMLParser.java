package org.example;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class HTMLParser {
    public static final String HTML_CODE_PATH = "Module17_Files/src/main/resources/code.html";

    public void parseHTMLDoc(){
        log.info("\nlist of skillbox courses:");
        String htmlDoc = parseFileToString(HTML_CODE_PATH);
        Document doc = Jsoup.parse(htmlDoc);
        Elements elementsH3 = doc.select("h3.ui-product-card-main__title");
        elementsH3.forEach(element -> System.out.println(element.text()));
    }

    public String parseFileToString(String path){
        StringBuilder result = new StringBuilder();
        try {
            List<String> lines = Files.readAllLines(Path.of(path));
            lines.forEach(line -> result.append(line).append("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result.toString();
    }

    // using Jsoup extract Document than extract image src by cssQuery(img) from Document
    // get Set of links from src
    public void downloadImagesFromWeb(){
        String urlSkillbox = "https://skillbox.ru/";
        String pathToDownload = "Module17_Files/images";
        Set<String> links = new HashSet<>();
        try {
            Document doc = Jsoup.connect(urlSkillbox).get();
            Elements images = doc.select("img");
            for (Element image : images){
                links.add(image.attr("abs:src"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int imageNumber = 0;
        for (String link : links){
            String extension = link.replaceAll("^.+\\.", "");
            String fileName = ++imageNumber + "." + extension;
            downloadByLink(link, fileName, pathToDownload);
        }
    }

    // uploading images via links using InputStream and FileOutputStream byte by byte
    private void downloadByLink(String link, String fileName, String pathToDownload) {
        try {
            URLConnection connection = new URL(link).openConnection();
            InputStream inputStream = connection.getInputStream();
            FileOutputStream outputStream =
                    new FileOutputStream(pathToDownload.concat("/").concat(fileName));
            int currentInputByte;
            while ((currentInputByte = inputStream.read()) != -1) {
                outputStream.write(currentInputByte);
            }
            log.info(fileName + " was downloaded from " + link);
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
