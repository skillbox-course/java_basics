package practice;

import java.util.Scanner;

public class TrucksAndContainers {

    public static final int NUMBER_OF_BOXES_IN_CONTAINER = 27;
    public static final int NUMBER_OF_CONTAINERS_IN_TRUCK = 12;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //получение количество коробок от пользователя
        int boxes = scanner.nextInt();

        // TODO: вывести в консоль коробки разложенные по грузовикам и контейнерам
        // пример вывода при вводе 2
        // для отступа используйте табуляцию - \t

        /*
        Грузовик: 1
            Контейнер: 1
                Ящик: 1
                Ящик: 2
        Необходимо:
        грузовиков - 1 шт.
        контейнеров - 1 шт.
        */

        //calculate required number of containers and trucks
        int requiredNumberOfContainers = boxes / NUMBER_OF_BOXES_IN_CONTAINER
                + ((boxes % NUMBER_OF_BOXES_IN_CONTAINER == 0) ? 0 : 1);
        int requiredNumberOfTrucks = requiredNumberOfContainers / NUMBER_OF_CONTAINERS_IN_TRUCK
                + ((requiredNumberOfContainers % NUMBER_OF_CONTAINERS_IN_TRUCK == 0) ? 0 : 1);

        //loops for output of the result - truck: container: box:
        int currentBox = 0;
        int currentContainer = 0;
        int currentTruck = 0;

        //output all boxes and checking the need output trucks or containers
        while (currentBox < boxes ) {
            if(currentBox % (requiredNumberOfContainers * requiredNumberOfTrucks) == 0)
                System.out.println("Грузовик: " + ++currentTruck);
            if(currentBox % requiredNumberOfContainers == 0)
                System.out.println("\tКонтейнер: " + ++currentContainer);
            System.out.println("\t\tЯщик: " + ++currentBox);
        }

        //final output of the required number of containers and trucks
        System.out.println("Необходимо:\nгрузовиков - "
                + requiredNumberOfTrucks + " шт."
                + "\nконтейнеров - " + requiredNumberOfContainers + " шт.");
    }

}
