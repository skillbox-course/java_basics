package practice;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Birthdays {


    public static void main(String[] args) {

        int day = 31;
        int month = 12;
        int year = 1990;

        System.out.println(collectBirthdays(year, month, day));

    }

    public static String collectBirthdays(int year, int month, int day) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(" - dd.MM.yyyy - EEE");
        LocalDate birthdayDate = LocalDate.of(year, month, day);
        LocalDate todayDate = LocalDate.now();
        //TODO реализуйте метод для построения строки в следующем виде
        //0 - 31.12.1990 - Mon
        //1 - 31.12.1991 - Tue
        String result = "";
        for (int i = 0; birthdayDate.isBefore(todayDate) || birthdayDate.isEqual(todayDate); i++) {
            result += String.format(System.lineSeparator() + i
                            + dateTimeFormatter.format(birthdayDate));
            birthdayDate = birthdayDate.plusYears(1);
        }
        return result;
    }
}
