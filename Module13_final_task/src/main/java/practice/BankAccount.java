package practice;

public class BankAccount {
  //amount of money in the account
  private double balance;

  public double getAmount() {
    return balance;
  }

  //depositing money to the account with checking the amount for a negative value
  public void put(double amountToPut) {
    if (amountToPut > 0){
      balance += amountToPut;
    } else {
      System.out.println("Amount to put less than 0");
    }
  }

  //withdrawal of money to the account with checking the balance
  public void take(double amountToTake) {
    if ((balance - amountToTake) >= 0){
      balance -= amountToTake;
    } else {
      System.out.println("Amount to take more than money in account");
    }
  }
}
