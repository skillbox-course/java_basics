package practice;

import java.time.LocalDate;

public class DepositAccount extends BankAccount {
    //date of last deposit
    private LocalDate lastIncome = LocalDate.now();

    //money withdrawal is possible only one month after lastIncome
    @Override
    public void take(double amountToTake) {
        if (lastIncome.plusMonths(1).isBefore(LocalDate.now())){
            super.take(amountToTake);
            lastIncome = LocalDate.now();
        } else {
            System.out.println("You can withdraw money once a month! Please withdraw the money after "
                    + lastIncome.plusMonths(1));
        }
    }
}
