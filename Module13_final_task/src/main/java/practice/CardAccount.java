package practice;

public class CardAccount extends BankAccount {
    private final double commission = 1.01;

    //withdrawing money including commissions
    @Override
    public void take(double amountToTake) {
        super.take(amountToTake * commission);
    }
}
