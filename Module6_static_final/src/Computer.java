import Computer_accessories.*;

public class Computer {
    private final String vendor;
    private final String name;
    private Processor processor;
    private RAM ram;
    private HardDrive hardDrive;
    private Monitor monitor;
    private Keyboard keyboard;
    private double computerWeight;

    public Computer(String vendor, String name) {
        this.vendor = vendor;
        this.name = name;
    }

    public Computer(String vendor, String name, Processor processor, RAM ram,
                    HardDrive hardDrive, Monitor monitor, Keyboard keyboard) {
        this.vendor = vendor;
        this.name = name;
        this.processor = processor;
        this.ram = ram;
        this.hardDrive = hardDrive;
        this.monitor = monitor;
        this.keyboard = keyboard;
        setComputerWeight();
    }

    @Override
    public String toString() {
        return "\nComputer{" +
                "vendor='" + vendor + '\'' +
                ", name='" + name + '\'' +
                "\n" + processor +
                "\n" + ram +
                "\n" + hardDrive +
                "\n" + monitor +
                "\n" + keyboard +
                "\ntotal PC weight=" + computerWeight +
                '}';
    }

    public void setComputerWeight() {
        this.computerWeight = processor.getWeight()
                        + ram.getWeight()+hardDrive.getHardDriveWeight()
                        + monitor.getMonitorWeight()+keyboard.getKeyboardWeight();
    }
    public void setComputerWeight(double weight){
        this.computerWeight = computerWeight + weight;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
        setComputerWeight(processor.getWeight());
    }

    public void setRam(RAM ram) {
        this.ram = ram;
        setComputerWeight(ram.getWeight());
    }

    public void setHardDrive(HardDrive hardDrive) {
        this.hardDrive = hardDrive;
        setComputerWeight(hardDrive.getHardDriveWeight());
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
        setComputerWeight(monitor.getMonitorWeight());
    }

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
        setComputerWeight(keyboard.getKeyboardWeight());
    }

    public String getVendor() {
        return vendor;
    }

    public String getName() {
        return name;
    }

    public double getComputerWeight() {
        return computerWeight;
    }

    public Processor getProcessor() {
        return processor;
    }

    public RAM getRam() {
        return ram;
    }

    public HardDrive getHardDrive() {
        return hardDrive;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }
}
