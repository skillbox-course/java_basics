package Computer_accessories;

public class Monitor {
    private final double monitorDiagonal;
    private final MonitorType monitorType ;
    private final double monitorWeight ;

    public Monitor(double monitorDiagonal, MonitorType monitorType, double monitorWeight) {
        this.monitorDiagonal = monitorDiagonal;
        this.monitorType = monitorType;
        this.monitorWeight = monitorWeight;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "monitorDiagonal=" + monitorDiagonal +
                ", monitorType=" + monitorType +
                ", monitorWeight=" + monitorWeight +
                '}';
    }

    public double getMonitorDiagonal() {
        return monitorDiagonal;
    }

    public MonitorType getMonitorType() {
        return monitorType;
    }

    public double getMonitorWeight() {
        return monitorWeight;
    }
}
