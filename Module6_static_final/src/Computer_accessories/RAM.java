package Computer_accessories;

public class RAM {
    private final RamType ramType;
    private final RamAmount ramAmount;
    private final double weight;

    public RAM(RamType ramType, RamAmount ramAmount, double weight) {
        this.ramType = ramType;
        this.ramAmount = ramAmount;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "RAM{" +
                "ramType=" + ramType +
                ", ramAmount=" + ramAmount +
                ", weight=" + weight +
                '}';
    }

    public RamType getRamType() {
        return ramType;
    }

    public RamAmount getRamAmount() {
        return ramAmount;
    }

    public double getWeight() {
        return weight;
    }
}
