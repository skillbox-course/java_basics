package Computer_accessories;

public class Keyboard {
    private final String keyboardType;
    private final boolean isHasKeyboardBlacklight;
    private final double keyboardWeight;

    public Keyboard(String keyboardType, boolean isHasKeyboardBlacklight, double keyboardWeight) {
        this.keyboardType = keyboardType;
        this.isHasKeyboardBlacklight = isHasKeyboardBlacklight;
        this.keyboardWeight = keyboardWeight;
    }

    @Override
    public String toString() {
        return "Keyboard{" +
                "keyboardType='" + keyboardType + '\'' +
                ", isHasKeyboardBlacklight=" + isHasKeyboardBlacklight +
                ", keyboardWeight=" + keyboardWeight +
                '}';
    }

    public String getKeyboardType() {
        return keyboardType;
    }

    public boolean isHasKeyboardBlacklight() {
        return isHasKeyboardBlacklight;
    }

    public double getKeyboardWeight() {
        return keyboardWeight;
    }
}
