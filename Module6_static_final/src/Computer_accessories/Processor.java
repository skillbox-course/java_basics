package Computer_accessories;

public class Processor {
    private final double frequency;
    private final ProcessorCoreNumber processorCoreNumber;
    private final String manufacturer;
    private final double weight;

    public Processor(double frequency, ProcessorCoreNumber processorCoreNumber, String manufacturer, double weight) {
        this.frequency = frequency;
        this.processorCoreNumber = processorCoreNumber;
        this.manufacturer = manufacturer;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Processor{" +
                "frequency=" + frequency +
                ", processorCoreNumber=" + processorCoreNumber +
                ", manufacturer='" + manufacturer + '\'' +
                ", weight=" + weight +
                '}';
    }

    public double getFrequency() {
        return frequency;
    }

    public ProcessorCoreNumber getProcessorCoreNumber() {
        return processorCoreNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getWeight() {
        return weight;
    }
}
