package Computer_accessories;

public class HardDrive {
    private final HardDriveType hardDriveType;
    private final int hardDriveCapacityGB;
    private final double hardDriveWeight;

    public HardDrive(HardDriveType hardDriveType, int hardDriveCapacityGB, double hardDriveWeight) {
        this.hardDriveType = hardDriveType;
        this.hardDriveCapacityGB = hardDriveCapacityGB;
        this.hardDriveWeight = hardDriveWeight;
    }

    @Override
    public String toString() {
        return "HardDrive{" +
                "hardDriveType=" + hardDriveType +
                ", hardDriveCapacityGB=" + hardDriveCapacityGB +
                ", hardDriveWeight=" + hardDriveWeight +
                '}';
    }

    public HardDriveType getHardDriveType() {
        return hardDriveType;
    }

    public int getHardDriveCapacityGB() {
        return hardDriveCapacityGB;
    }

    public double getHardDriveWeight() {
        return hardDriveWeight;
    }
}
