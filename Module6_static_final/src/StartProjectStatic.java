import Computer_accessories.*;

public class StartProjectStatic {
    public static void main(String[] args) {
        Computer computer = new Computer("AMD","model 1",
                new Processor(2.6, ProcessorCoreNumber.OCTA_CORE,"AMD",0.102),
                new RAM(RamType.DDR4, RamAmount.GB_8,0.016),
                new HardDrive(HardDriveType.SSD,320,0.073),
                new Monitor(23.8,MonitorType.IPS,3.8),
                new Keyboard("bluetooth",false,0.535));
        System.out.println(computer);

        Computer computer2 = new Computer("Intel","model1");
        computer2.setProcessor(new Processor(3.2,ProcessorCoreNumber.DUAL_CORE,
                "Intel",0.96));
        System.out.println(computer2);
    }
}
