package org.example;

import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) throws IOException {

        long start = System.currentTimeMillis();
        char[] letters = {'Y', 'K', 'E', 'H', 'X', 'B', 'A', 'P', 'O', 'C', 'M', 'T'};
        // PrintWriter в отличие от FileOutputStream сам проверяет заполненность буфера
        // и записывает данные когда нужно
        PrintWriter writer = new PrintWriter("src/main/data/result.txt");

        for (int regionCode = 1; regionCode < 50; regionCode++) {
            StringBuilder result = new StringBuilder();
            for (int number = 1; number < 1000; number++) {
                for (char firstLetter : letters) {
                    for (char secondLetter : letters) {
                        for (char thirdLetter : letters) {
                            result.append(firstLetter).append(padNumber(number, 3))
                                    .append(secondLetter).append(thirdLetter)
                                    .append(padNumber(regionCode, 2)).append("\n");
                        }
                    }
                }
            }writer.write(result.toString());
        }
        writer.flush();
        writer.close();
        System.out.println(System.currentTimeMillis() - start);
    }

    private static String padNumber(int number, int numberLength) {
        StringBuilder numberStr = new StringBuilder(Integer.toString(number));
        int padSize = numberLength - numberStr.length();
        for (int i = 0; i < padSize; i++) {
            numberStr.append('0').append(numberStr);
        }
        return numberStr.toString();
    }
}























