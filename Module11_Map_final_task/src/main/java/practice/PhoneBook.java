package practice;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class PhoneBook {
    //treeMap<phone, name>
    private TreeMap<String, String> phoneBook = new TreeMap<>();

    public boolean hasPhoneNumber(String number) {
        return phoneBook.containsKey(number);
    }
    public boolean hasPhoneName(String name) {
        return phoneBook.containsValue(name);
    }
    public boolean isPhoneNumber(String str) {
        return str.matches("[7]{1}[0-9]{10}");
    }
    public boolean isPhoneName(String str) {
        return str.matches("[А-яЁё]{1,15}");
    }

    public void addContact(String phone, String name) {
        // проверьте корректность формата имени и телефона
        // (рекомедуется написать отдельные методы для проверки является строка именем/телефоном)
        // если такой номер уже есть в списке, то перезаписать имя абонента
        if (isPhoneNumber(phone) && isPhoneName(name)) {
            phoneBook.put(phone, name);
        }
    }

    public String getContactByPhone(String phone) {
        // формат одного контакта "Имя - Телефон"
        // если контакт не найдены - вернуть пустую строку
        return phoneBook.containsKey(phone) ? phoneBook.get(phone).concat(" - ").concat(phone) : "";
    }

    //check every entry in phoneBook for equals name
    public Set<String> getContactByName(String name) {
        // формат одного контакта "Имя - Телефон"
        // если контакт не найден - вернуть пустой TreeSet
        Set<String> result = new TreeSet<>();
        if (!phoneBook.containsValue(name)){
            return result;
        }
        StringBuilder contact = new StringBuilder(name).append(" - ");
        for (Map.Entry<String, String> entry : phoneBook.entrySet()){
            if (entry.getValue().equals(name)){
                contact.append(entry.getKey());
                contact.append(", ");
            }
        }
        result.add(contact.substring(0,contact.length()-2));
        return result;
    }

    //call getContactByName method for every entry in phoneBook
    public Set<String> getAllContacts() {
        // формат одного контакта "Имя - Телефон"
        // если контактов нет в телефонной книге - вернуть пустой TreeSet
        if (phoneBook.isEmpty()) {
            return new TreeSet<>();
        }
        Set<String> result = new TreeSet<>();
        for (Map.Entry<String, String> entry : phoneBook.entrySet()){
            result.addAll(getContactByName(entry.getValue()));
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Set<String> set = getAllContacts();
        for (String str : set){
            result.append(str).append("\n");
        }
        return result.toString().trim();
    }

    // для обхода Map используйте получение пары ключ->значение Map.Entry<String,String>
    // это поможет вам найти все ключи (key) по значению (value)
    /*
        for (Map.Entry<String, String> entry : map.entrySet()){
            String key = entry.getKey(); // получения ключа
            String value = entry.getValue(); // получения ключа
        }
    */
}