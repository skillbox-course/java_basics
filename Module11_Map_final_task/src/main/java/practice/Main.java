package practice;

import java.util.Scanner;

public class Main {
    //Phone number, name or command is entered into console.
    //If phoneBook does not contain the name or number, they are requested and stored in the phoneBook
    //Command LIST to output phoneBook
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PhoneBook phoneBook = new PhoneBook();

        while (true) {
            System.out.println("\nВведите номер, имя или команду:");
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("q")){break;}
            //Check is input phone number.
            //If number is missing, name is requested and stored to the phoneBook
            if (phoneBook.isPhoneName(input)){
                if (phoneBook.hasPhoneName(input)){
                    System.out.println(phoneBook.getContactByName(input));
                } else {
                    String phoneName = input;
                    System.out.println("Такого имени в телефонной книге нет.");
                    System.out.println("Введите номер телефона для абонента “"+phoneName+"”:");
                    input = scanner.nextLine();
                    phoneBook.addContact(input, phoneName);
                    System.out.println("Контакт сохранен!");
                }
                continue;
            }
            //Check is input phone name.
            //If name is missing, number is requested and stored to the phoneBook
            if (phoneBook.isPhoneNumber(input)){
                if (phoneBook.hasPhoneNumber(input)){
                    System.out.println(phoneBook.getContactByPhone(input));
                } else {
                    String phone = input;
                    System.out.println("Такого номера нет в телефонной книге.");
                    System.out.println("Введите имя абонента для номера “"+phone+"”:");
                    input = scanner.nextLine();
                    phoneBook.addContact(phone, input);
                    System.out.println("Контакт сохранен!");
                }
                continue;
            }
            //command list for output phonebook
            if (input.toUpperCase().matches("LIST")){
                System.out.println(phoneBook);
                continue;
            }
            System.out.println("Неверный формат ввода");
        }
    }
}
