package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.Method;

class CarTest {
    Car car;

    @BeforeEach
    void prepareData(){
        car = new Car("camry", "ABC-123", "Ivan");
    }

    @Test
    void getModel() {
        Assertions.assertEquals("camry", car.getModel());
    }

    @Test
    void setModel() {
        car.setModel("prius");
        Assertions.assertEquals("camry", car.getModel());
    }

    @Test
    void setModelIfModelEmpty() {
        Car currentCar = new Car("", "ABC-123", "Ivan");
        currentCar.setModel("prius");
        Assertions.assertEquals("prius", currentCar.getModel());
    }

    @Test
    void getNumber() {
        Assertions.assertEquals("ABC-123", car.getNumber());
    }

    @ParameterizedTest
    @ValueSource(strings = {"ABC-123", "123-abc"})
    @NullSource @EmptySource
    void setNumberMultipleValues(String number) {
        car.setNumber(number);
        Assertions.assertEquals(number, car.getNumber());
    }

    @ParameterizedTest
    @CsvSource({"'ABC-123', 'ABC-123'",
    "'DEF-456','DEF-456'"})
    void setNumberMultipleValues(String number, String result) {
        car.setNumber(number);
        Assertions.assertEquals(result, car.getNumber());
    }

    @ParameterizedTest
    @CsvSource({"1,5","0,4"})
    void sumTest(int a, int result){
        Assertions.assertEquals(result, a + 4);
    }

    @Test
    void getOwner() {
        Assertions.assertEquals("Ivan", car.getOwner());
    }

    @Test
    void setOwner() {
        car.setOwner("Petr");
        Assertions.assertEquals("Petr", car.getOwner());
    }

    @Test
    void getOwners() {
        Assertions.assertArrayEquals(new String[] {"Ivan"}, car.getOwners().toArray());
    }

    @Test
    void getListTwoOwners(){
        car.setOwner("Petr");
        Assertions.assertArrayEquals(new String[] {"Ivan", "Petr"}, car.getOwners().toArray());
    }

    @Test
    void privateMethodTest(){
        try {
            Method currentMethod = Car.class
                    .getDeclaredMethod("testPrivateMethod");
            currentMethod.setAccessible(true);
            Assertions.assertEquals(currentMethod.invoke(car).toString(), "toyota");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void privateMethodTestWithStringParam(){
        try {
            Method currentMethod = Car.class
                    .getDeclaredMethod("testPrivateMethod", String.class);
            currentMethod.setAccessible(true);
            Assertions.assertEquals(currentMethod.invoke(car, "prius").toString(), "toyota prius");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void outputToConsole() {
    }
}