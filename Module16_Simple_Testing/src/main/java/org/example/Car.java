package org.example;

import java.util.ArrayList;
import java.util.List;

public class Car {
    private String model;
    private String number;
    private String owner;
    private List<String> owners = new ArrayList<>();

    public Car(String model, String number, String owner) {
        this.model = model;
        this.number = number;
        this.owner = owner;
        owners.add(owner);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (this.model.isEmpty()){
            this.model = model;
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        owners.add(owner);
    }

    public List<String> getOwners() {
        return owners;
    }

    private String testPrivateMethod(){
        return "toyota";
    }

    private String testPrivateMethod(String str){
        return "toyota " + str;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", number='" + number + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }

    public void outputToConsole(){
        System.out.println(this);
    }
}