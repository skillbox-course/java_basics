import Delivery.Cargo;
import Delivery.Dimensions;
import Elevator.Elevator;

import java.util.Scanner;

public class StartEncapsulationProject {
    public static void main(String[] args) {

        Dimensions cargo1Dimensions = new Dimensions(3,4,5);
        Cargo cargo1 = new Cargo(2.5,"Moscow",
                false,"34543465345",
                false,cargo1Dimensions);
        System.out.println("cargo1 hashcode "+cargo1.hashCode());
        System.out.println(cargo1);
        Cargo cloneCargo1 = cargo1.cloneCargoWithNewDimensions(cargo1Dimensions.setCargoHeight(2));
        System.out.println("cloneCargo1 hashcode "+cloneCargo1.hashCode());
        System.out.println(cloneCargo1);

        Elevator elevator = new Elevator(-3, 26);
        while (true) {
            System.out.print("\nВведите номер этажа: ");
            int floor = new Scanner(System.in).nextInt();
            elevator.move(floor);
        }
    }
}
