package Elevator;

public class Elevator {
    private int currentFloor = 1;
    private final int minFloor;
    private final int maxFloor;
    private final String warningAboutFloors;

    public Elevator(int minFloor, int maxFloor) {
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        warningAboutFloors ="\nbottom floor "+minFloor+" top floor "+maxFloor;
        System.out.println(warningAboutFloors);
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void moveDown() {
        currentFloor = currentFloor - 1;
    }
    public void moveUp() {
        currentFloor = currentFloor + 1;
    }
    public void move(int floor) {
        if (floor < minFloor || floor >maxFloor){
            System.out.println("incorrect floor number"+warningAboutFloors);
        }else {
            for (; currentFloor != floor; ) {
                if (currentFloor < floor) moveUp();
                if (currentFloor > floor) moveDown();
                System.out.println(currentFloor);
            }
        }
    }
}
