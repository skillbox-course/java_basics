package Delivery;

public class Dimensions {
    private final int cargoHeight;
    private final int cargoWidth;
    private final int cargoLength;

    public Dimensions(int cargoHeight, int cargoWidth, int cargoLength) {
        this.cargoHeight = cargoHeight;
        this.cargoWidth = cargoWidth;
        this.cargoLength = cargoLength;
    }

    @Override
    public String toString() {
        return "Dimensions{" +
                "cargoHeight=" + cargoHeight +
                ", cargoWidth=" + cargoWidth +
                ", cargoLength=" + cargoLength +
                ", cargoVolume=" + getCargoVolume()+
                '}';
    }
    public Dimensions setCargoHeight(int cargoHeight){
        return new Dimensions(cargoHeight,cargoWidth,cargoLength);
    }
    public Dimensions setCargoWidth(int cargoWidth){
        return new Dimensions(cargoHeight,cargoWidth,cargoLength);
    }
    public Dimensions setCargoLength(int cargoLength){
        return new Dimensions(cargoHeight,cargoWidth,cargoLength);
    }
    public int getCargoVolume() {
        return cargoHeight*cargoWidth*cargoLength;
    }
    public int getCargoHeight() {
        return cargoHeight;
    }
    public int getCargoWidth() {
        return cargoWidth;
    }
    public int getCargoLength() {
        return cargoLength;
    }
}
