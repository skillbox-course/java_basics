package Delivery;

public class Cargo {
    private final double cargoWeight;
    private final String cargoAddress;
    private final boolean isPossibleToFlip;
    private final String cargoRegistrationNumber;
    private final boolean isTheCargoFragile;
    private final Dimensions cargoDimensions;

    public Cargo(double cargoWeight, String cargoAddress,
                 boolean isPossibleToFlip, String cargoRegistrationNumber,
                 boolean isTheCargoFragile, Dimensions cargoDimensions) {
        this.cargoWeight = cargoWeight;
        this.cargoAddress = cargoAddress;
        this.isPossibleToFlip = isPossibleToFlip;
        this.cargoRegistrationNumber = cargoRegistrationNumber;
        this.isTheCargoFragile = isTheCargoFragile;
        this.cargoDimensions = cargoDimensions;
    }

    @Override
    public String toString() {
        return "Cargo{" +
                "cargoWeight=" + cargoWeight +
                ", cargoAddress='" + cargoAddress + '\'' +
                ", isPossibleToFlip=" + isPossibleToFlip +
                ", cargoRegistrationNumber='" + cargoRegistrationNumber + '\'' +
                ", isTheCargoFragile='" + isTheCargoFragile + '\'' +
                ", cargoDimensions=" + cargoDimensions +
                '}';
    }

    public Cargo cloneCargoWithNewWeight(double cargoWeight){
        return new Cargo(cargoWeight,cargoAddress,isPossibleToFlip,
                cargoRegistrationNumber,isTheCargoFragile,cargoDimensions);
    }
    public Cargo cloneCargoWithNewAddress(String cargoAddress){
        return new Cargo(cargoWeight,cargoAddress,isPossibleToFlip,
                cargoRegistrationNumber,isTheCargoFragile,cargoDimensions);
    }
    public Cargo cloneCargoWithNewDimensions(Dimensions cargoDimensions){
        return new Cargo(cargoWeight,cargoAddress,isPossibleToFlip,
                cargoRegistrationNumber,isTheCargoFragile,cargoDimensions);
    }

    public double getCargoWeight() {
        return cargoWeight;
    }

    public String getCargoAddress() {
        return cargoAddress;
    }

    public boolean isPossibleToFlip() {
        return isPossibleToFlip;
    }

    public String getCargoRegistrationNumber() {
        return cargoRegistrationNumber;
    }

    public boolean getIsTheCargoFragile() {
        return isTheCargoFragile;
    }

    public Dimensions getCargoDimensions() {
        return cargoDimensions;
    }
}
