package practice.twoDimensionalArray;

public class TwoDimensionalArray {

    public static final char SYMBOL = 'X';

    public static char[][] getTwoDimensionalArray(int size) {

        //TODO: Написать метод, который создаст двумерный массив char заданного размера.
        // массив должен содержать символ SYMBOL по диагоналям, пример для size = 3
        // [X,  , X]
        // [ , X,  ]
        // [X,  , X]
        char[][] testArray = new char[size][size];
        //passing through all elements of the array
        for (int row = 0; row < size; row++){
            for (int column = 0; column < size; column++){
                //is the element part of the diagonal
                testArray[row][column] = ((row == column) || (row + column == size - 1)) ?
                        SYMBOL : ' ';
            }
        }
        return testArray;
    }
}
