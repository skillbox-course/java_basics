package practice.hospital;

public class Hospital {
    private static float[] temperatureData;
    public static float[] generatePatientsTemperatures(int patientsCount) {
        temperatureData = new float[patientsCount];
        float patientTemperature;
        for(int i = 0; i < patientsCount; i++){
            //generate random patientTemperature from 32 to 40
            patientTemperature = (float) Math.random() * (40 - 32) + 32;
            //rounding patientTemperature and putt it into array
            temperatureData[i] = (float)(Math.round(patientTemperature * 10) / 10.0);
        }
        return temperatureData;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
            Округлите среднюю температуру с помощью Math.round до 2 знаков после запятой,
            а температуры каждого пациента до 1 знака после запятой
        */
        float allPatientsTotalTemperature = 0;
        int healthyPatientCount = 0;
        StringBuilder temperatureDataToString = new StringBuilder();
        for ( int i = 0; i < temperatureData.length; i++ ){
            //calculate healthy patient count
            if (temperatureData[i] >= 36.2f && temperatureData[i] <= 36.9f) {
                healthyPatientCount++ ;
            }
            allPatientsTotalTemperature += temperatureData[i];
            //create string of all patients temperatures
            temperatureDataToString.append(i == (temperatureData.length - 1) ?
                    temperatureData[i] : temperatureData[i] + " ");
        }
        //calculate average hospital temperature
        float averageHospitalTemperature =
                (float)Math.round((allPatientsTotalTemperature / temperatureData.length) * 100) / 100;

        String report =
            "Температуры пациентов: " + temperatureDataToString +
            "\nСредняя температура: " + averageHospitalTemperature +
            "\nКоличество здоровых: " + healthyPatientCount;

        return report;
    }
}
