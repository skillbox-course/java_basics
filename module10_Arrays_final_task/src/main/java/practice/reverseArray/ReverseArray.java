package practice.reverseArray;

public class ReverseArray {

    public static String[] reverse(String[] strings) {
        //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
        if(strings.length == 0) return strings;
        int lastElementIndex = strings.length - 1;
        String tmpStr;
        //loop for change array elements
        for (int i = 0; i <= (lastElementIndex) / 2; i++){
            tmpStr = strings[i];
            strings[i] = strings[lastElementIndex - i];
            strings[lastElementIndex - i] = tmpStr;
        }
        return strings;
    }

}