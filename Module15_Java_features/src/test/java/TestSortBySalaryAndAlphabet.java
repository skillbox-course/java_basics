import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@DisplayName("Сортировка по заработной плате и алфавиту")
public class TestSortBySalaryAndAlphabet {

    @Test
    @DisplayName("Входные данные - staff.txt")
    void sort() {
        List<Employee15> actualStaff = Employee15.loadStaffFromFile("data/staff.txt");
        List<Employee15> expectedStaff = Employee15.loadStaffFromFile("data/sortedStaff.txt");
        new EmployeeUtils().sortBySalaryAndAlphabet(actualStaff);
        assertIterableEquals(expectedStaff, actualStaff, "сортировка выполнена не по условию");
    }

}
