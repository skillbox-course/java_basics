import java.util.ArrayList;

public class LRUCache <T>{
    private ArrayList<T> elements;
    private int size;

    public LRUCache(int size) {
        this.size = size;
        elements = new ArrayList<>();
    }

    public void addElements(T element){
        if (elements.size() >= size){
            for (int i = 0; i < (elements.size() - size + 1); i++){
                elements.remove(i);
            }
        }
        elements.add(element);
    }

    public T getElement(int index){
        if (index >= size){
            return null;
        }
        return elements.get(index);
    }

    public ArrayList<T> getElements() {
        return elements;
    }
}