import lombok.AllArgsConstructor;
import lombok.Data;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Employee15 {
    private String name;
    private Integer salary;
    private LocalDate workStart;

    private static DateTimeFormatter dateFormatter =
        DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public static List<Employee15> loadStaffFromFile(String path) {
        List<Employee15> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for (String line : lines) {
                String[] fragments = line.split("\t");
                if (fragments.length != 3) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }

                staff.add(new Employee15(
                    fragments[0],
                    Integer.parseInt(fragments[1]),
                    LocalDate.parse(fragments[2], dateFormatter)
                ));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }

    public String toString() {
        return name + " - " + salary + " - " +
            dateFormatter.format(workStart);
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (o == null || getClass() != o.getClass()) {
//            return false;
//        }
//        Employee15 employee = (Employee15) o;
//        return Objects.equals(name, employee.name) &&
//                Objects.equals(salary, employee.salary) &&
//                Objects.equals(workStart, employee.workStart);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(name, salary, workStart);
//    }

}
