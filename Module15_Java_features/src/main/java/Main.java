import java.util.List;

public class Main {

    public static final String STAFF_TXT = "Module15_Java_features/data/staff.txt";

    public static void main(String[] args) {
        List<Employee15> staff = Employee15.loadStaffFromFile(STAFF_TXT);
        EmployeeUtils employeeUtils = new EmployeeUtils();
        // sort staff by salary and alphabet
        employeeUtils.sortBySalaryAndAlphabet(staff);
        // output staff to console
        employeeUtils.outputEmployeesToConsole(staff);
        employeeUtils.increaseSalaryBy(staff, 10_000);
        // output employee with salary more than 120_000
        employeeUtils.outputEmployeeWithSalaryMoreThan(staff, 120_000);
        // extract even numbers
        employeeUtils.extractEvenNumbers(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).stream());
//        employeeUtils.createEndlessStream();
        // sort by name
        employeeUtils.sortedByName(staff);
        // employee with max salary
        employeeUtils.employeeWithMaxSalary(staff);
        // get employees with salary more than
        employeeUtils.getEmployeesWithSalaryMoreThan(staff, 100_000)
                .stream().forEach(System.out::println);
        // is none employee with salary more than
        System.out.println(employeeUtils.isNoneEmployeeWithSalaryMoreThan(staff, 150_000));
        // grouping staff by salary
        employeeUtils.groupingBySalary(staff);

        System.out.println("\nEmployee who came in the N year and have max salary");
        System.out.println(employeeUtils.findEmployeeWithHighestSalary(staff, 2018));
        // sum of salary bigger than 100_000
        employeeUtils.sumOfSalaryBiggerThan(staff, 100_000);
        // create LRU cache for practice with generics
        LRUCache<Employee15> employeesCache = new LRUCache<>(6);
        staff.stream().forEach((Employee15 e) -> employeesCache.addElements(e));
        System.out.println("\nEmployee cache contains ");
        employeesCache.getElements().stream().forEach(System.out::println);

    }
}