import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeUtils {

    public void outputEmployeesToConsole(List<Employee15> staff){
        staff.forEach(System.out::println);
    }

    public void increaseSalaryBy(List<Employee15> staff, int amountOfIncrease){
        staff.forEach((Employee15 e) -> e.setSalary(e.getSalary() + amountOfIncrease));
        System.out.println("\nIncrease salary by " + amountOfIncrease);
        staff.forEach(System.out::println);
    }

    public void outputEmployeeWithSalaryMoreThan(List<Employee15> staff, int salaryBorder){
        System.out.println("\nStream employees who have salary more than " + salaryBorder);
        Stream<Employee15> employee15Stream = staff.stream()
                .filter((Employee15 e) -> e.getSalary() >= salaryBorder);
        employee15Stream.forEach(System.out::println);
    }

    public void sortedByName(List<Employee15> staff){
        System.out.println("\nSorted by name");
        staff.stream().sorted(Comparator.comparing((Employee15 e) -> e.getName())
                .reversed())
                .forEach(System.out::println);
    }

    public void employeeWithMaxSalary(List<Employee15> staff){
        System.out.println("\nEmployee with max salary");
        staff.stream().max(Comparator.comparing((Employee15::getSalary)))
                .ifPresent(System.out::println);
    }

    public List<Employee15> getEmployeesWithSalaryMoreThan(List<Employee15> staff, int salaryLowerBorder){
        System.out.println("\nList employees who have salary more than " + salaryLowerBorder);
        return staff.stream()
                .filter((Employee15 e) -> e.getSalary() >= salaryLowerBorder)
                .collect(Collectors.toList());
    }

    public boolean isNoneEmployeeWithSalaryMoreThan(List<Employee15> staff, int salaryBorder){
        System.out.print("\nAre there NONE employees who have salary bigger than 150_000 ");
        return staff.stream().noneMatch((Employee15 e) -> e.getSalary() > salaryBorder);
    }

    public void groupingBySalary(List<Employee15> staff){
        System.out.println("\nEmployees grouping by salary");
        Map<Integer, List<Employee15>> employeeMap = staff.stream()
                .collect(Collectors.groupingBy((Employee15 e) -> e.getSalary()));
        employeeMap.forEach((Integer i, List<Employee15> e) -> {
            System.out.println(i);
            e.forEach(System.out::println);
        });
    }

    public void sumOfSalaryBiggerThan(List<Employee15> staff, int salaryBorder){
        System.out.println("\nSum of all employee salaries that are higher than " + salaryBorder);
        staff.stream().map((Employee15 e) -> e.getSalary())
                .filter((Integer salary) -> salary >= salaryBorder)
                .reduce((Integer result, Integer salary) -> result + salary)
                .ifPresent(System.out::println);
    }

    public void createEndlessStream(){
//        Stream.iterate(1, n -> n + 1).forEach(System.out::println);
        Stream.generate(() -> "abc").forEach(System.out::println);
    }

    public void sortBySalaryAndAlphabet(List<Employee15> staff) {
        //TODO Метод должен отсортировать сотрудников по заработной плате и алфавиту.
        Collections.sort(staff, Comparator.comparing(Employee15::getSalary)
                .thenComparing(Employee15::getName));
        Collections.sort(staff, Comparator.comparing((Employee15 employee15) -> employee15.getSalary())
                .thenComparing(employee15 -> employee15.getName()));
    }

    public Employee15 findEmployeeWithHighestSalary(List<Employee15> staff, int year) {
        //TODO Метод должен вернуть сотрудника с максимальной зарплатой среди тех,
        // кто пришёл в году, указанном в переменной year
        return staff.stream()
                .filter((Employee15 e) -> e.getWorkStart().getYear() == year)
                .max(Comparator.comparing((Employee15 e) -> e.getSalary()))
                .get();
    }

    public void extractEvenNumbers(Stream<Integer> numbers){
        System.out.println("\nstream of numbers multiplies of 2");
        numbers.filter((Integer i) -> i % 2 == 0).forEach(System.out::println);
    }
}
