import java.util.ArrayList;

public class ListExperiments {
    int winnersRate = 10;
    int certCount = 100;
    int[] certNumbers = new int[certCount];
    boolean[] certIsWin = new boolean[winnersRate];
    ArrayList<Integer> winnersCerts = new ArrayList<>();
    public void createCertificatesUsingList(){
        for (int i = 0; i < certCount; i++){
            certNumbers[i] = (int)Math.round(Math.random() * 8_999_999) + 1_000_000;
            if (i % winnersRate == 0) winnersCerts.add(certNumbers[i]);
        }
    }
    public void createCertificates(){
        for (int i = 0; i < certCount; i++){
            certNumbers[i] = (int)Math.round(Math.random() * 8_999_999) + 1_000_000;
            certIsWin[i] = i % winnersRate == 0;
        }
    }
}
