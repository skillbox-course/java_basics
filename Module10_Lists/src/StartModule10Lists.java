import java.util.ArrayList;
import java.util.Arrays;

public class StartModule10Lists {
    public static void main(String[] args) {

        Integer[] intNumbers = new Integer[]{10,20,30,40};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(intNumbers));
        System.out.println(arrayList);
        Integer[] numbers2 = arrayList.toArray(new Integer[0]);
        System.out.println(Arrays.toString(numbers2));

        ArrayList<Task> taskList = new ArrayList<>();
        taskList.add(new Task("buy a milk","three packages"));
        taskList.add(new Task("feed the cat","fresh food"));
        taskList.add(new Task("burn the secret docs","from green folder"));
        taskList.add(1, new Task("super important task", ""));
        taskList.set(2, new Task("feed dog", "fresh food"));
        System.out.println(taskList);

        ListExperiments listExperiments = new ListExperiments();
        listExperiments.createCertificatesUsingList();
        System.out.println(listExperiments.winnersCerts);
    }
}
