public class ArrayExperiments {
    public Product[] products = new Product[]{
            new Product("milk", 75),
            new Product("bread", 50),
            new Product("cheese", 180),
            new Product("teapot", 1520),
            new Product("water filter", 1180)};
    int certificateCount = 100;
    int winnersRate = 10;
    int[] certificate = new int[certificateCount];
    int[] winnersNumbers = new int[certificate.length / winnersRate];
    double productDiscount = 0.1;
    //calculate discount for products, which price > 1000. Products without setters
    public void productsDiscount(Product[] products){
        for (int i = 0; i < products.length; i++){
            if (products[i].getPrice() > 1000){
                int discountPrice = (int)(products[i].getPrice() * (1 - productDiscount));
                products[i] = new Product(products[i].getName(),discountPrice);
            }
            System.out.println(products[i]);
        }
    }
    //create array of random seven - digit certificates
    public void initArray(){
        System.out.println();
        for (int i = 0; i < certificate.length; i++){
            certificate[i] = 1_000_000 + (int)(8_999_999 * Math.random());
            System.out.println(certificate[i]);
        }
    }
    //calculate winner-certificate. It's every winnersRate certificate
    public void calculateWinners(){
        System.out.println("======================");
        for (int i = 0; i < winnersNumbers.length; i ++){
            winnersNumbers[i] = certificate[i * winnersRate];
            System.out.println(winnersNumbers[i]);
        }
    }

}
