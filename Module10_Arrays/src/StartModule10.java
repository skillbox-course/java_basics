import chess.Chessboard;
import java.util.Arrays;

public class StartModule10 {
    public static void main(String[] args) {

        int[] testArray = new int[]{455, 6256, 23, 164, 452};
        int[] testArray2 = new int[]{455, 6256, 23, 2367};
        System.out.println("\ntestArray toString "+Arrays.toString(testArray));
        System.out.println("testArray2 toString "+Arrays.toString(testArray2));
        System.out.println("compare testArray and testArray2 = "+Arrays.compare(testArray,testArray2));
        testArray2 = Arrays.copyOf(testArray, 5);
        System.out.println("testArray2 copy 5 elements testArray"+Arrays.toString(testArray2));
        Arrays.sort(testArray);
        System.out.println("sorted testArray "+Arrays.toString(testArray));
        Arrays.fill(testArray,2,4,-5);
        System.out.println("testArray filled -5 "+Arrays.toString(testArray));

        Chessboard chessboard = new Chessboard();
        System.out.println(chessboard);

        ArrayExperiments arrayExperiments = new ArrayExperiments();
        System.out.println("\n" + Arrays.toString(arrayExperiments.products));
        System.out.println("=============");
        arrayExperiments.productsDiscount(arrayExperiments.products);
        arrayExperiments.initArray();
        arrayExperiments.calculateWinners();
    }
}
