package chess;

public class Chessboard {
    //initialization chessboard - two-dimension array
    Figure[][] chessboard = new Figure[8][];
    public Chessboard() {
        createChessboard();
    }
    public void createChessboard(){
        //initialization all arrays - create all chessboard row
        for (int i = 0; i < chessboard.length; i++){
            chessboard[i] = new Figure[8];
        }
        chessboard[0] = new Figure[]{
                new Figure(FigureColor.WHITE, FigureType.ROOK),
                new Figure(FigureColor.WHITE, FigureType.KNIGHT),
                new Figure(FigureColor.WHITE, FigureType.BISHOP),
                new Figure(FigureColor.WHITE, FigureType.QUEEN),
                new Figure(FigureColor.WHITE, FigureType.KING),
                new Figure(FigureColor.WHITE, FigureType.BISHOP),
                new Figure(FigureColor.WHITE, FigureType.KNIGHT),
                new Figure(FigureColor.WHITE, FigureType.ROOK),
        };
        chessboard[7] = new Figure[]{
                new Figure(FigureColor.BLACK, FigureType.ROOK),
                new Figure(FigureColor.BLACK, FigureType.KNIGHT),
                new Figure(FigureColor.BLACK, FigureType.BISHOP),
                new Figure(FigureColor.BLACK, FigureType.QUEEN),
                new Figure(FigureColor.BLACK, FigureType.KING),
                new Figure(FigureColor.BLACK, FigureType.BISHOP),
                new Figure(FigureColor.BLACK, FigureType.KNIGHT),
                new Figure(FigureColor.BLACK, FigureType.ROOK),
        };
        for (int i = 0; i < chessboard.length; i++){
            chessboard[1][i] = new Figure(FigureColor.WHITE, FigureType.PAWN);
            chessboard[6][i] = new Figure(FigureColor.BLACK, FigureType.PAWN);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        //filling in all fields of the chessboard
        for (Figure[] row : chessboard){
            for (int collumn = 0; collumn < chessboard.length; collumn++){
                stringBuilder.append(row[collumn] == null ? "---" : row[collumn]);
                stringBuilder.append(collumn < (chessboard.length - 1) ? " " : "\n");
            }
        }
        return stringBuilder.toString();
    }
}
