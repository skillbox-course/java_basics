package chess;

public class Figure {
    FigureColor figureColor;
    FigureType figureType;

    public Figure(FigureColor figureColor, FigureType figureType) {
        this.figureColor = figureColor;
        this.figureType = figureType;
    }

    public FigureColor getFigureColor() {
        return figureColor;
    }

    public FigureType getFigureType() {
        return figureType;
    }

    @Override
    public String toString() {
        String result = figureColor.toString().substring(0,1)
                .concat(figureType.toString().substring(0,2));
        return result;
    }
}
