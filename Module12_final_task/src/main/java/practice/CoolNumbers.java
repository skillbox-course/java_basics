package practice;

import java.util.*;

//creating list of car numbers and several methods of searching for items in various ways
public class CoolNumbers {
    private static final ArrayList<String> COOL_NUMBERS = new ArrayList<>();
    private static final ArrayList<Character> CAR_NUMBER_LETTERS = new ArrayList<>(
            Arrays.asList('А', 'В', 'С', 'Е', 'Н', 'К', 'М', 'О', 'Р', 'Т', 'Х', 'У'));
    private static final int CAR_NUMBER_COUNT = 2_000_000;

    public static boolean isCarNumber(String carNumber){
        String regex = CAR_NUMBER_LETTERS + "\\d{3}" + CAR_NUMBER_LETTERS + CAR_NUMBER_LETTERS + "\\d{2,3}";
        return carNumber.matches(regex);
    }

    public static String generateCarNumber(){
        //initialization variables
        Collections.shuffle(CAR_NUMBER_LETTERS);
        StringBuilder carNumber = new StringBuilder();
        int digitForNumber = (int)(Math.random()*(10-1) + 1);
        String str = (int)(Math.random() * 1000 + 1) + "";
        str = ((str.length() == 1) ? "0".concat(str) : str);
        //create car number. Three middle digits are the same, all letters are different
        carNumber.append(CAR_NUMBER_LETTERS.get(0));
        carNumber.append(digitForNumber).append(digitForNumber).append(digitForNumber);
        carNumber.append(CAR_NUMBER_LETTERS.get(1)).append(CAR_NUMBER_LETTERS.get(2));
        carNumber.append(str);
        return carNumber.toString();
    }

    public static List<String> generateCoolNumbers() {
        for (int i = 0; i <= CAR_NUMBER_COUNT; i++){
            COOL_NUMBERS.add(generateCarNumber());
        }
        return COOL_NUMBERS;
    }

    //brute force search with time measurement
    public static boolean bruteForceSearchInList(List<String> list, String number) {
        long methodStart = System.nanoTime();                                       //start time measurement
        for (String s : list) {
            if (s.equals(number)) {
                System.out.println("Поиск перебором: номер найден, поиск занял " +
                        (System.nanoTime() - methodStart) + "нс");
                return true;
            }
        }
        System.out.println("Поиск перебором: номер не найден, поиск занял " +
                (System.nanoTime() - methodStart) + "нс");
        return false;
    }

    //sorting collection and binary search with time measurement
    public static boolean binarySearchInList(List<String> sortedList, String number) {
        Collections.sort(sortedList);                                               //sorting collection for binary search
        long methodStart = System.nanoTime();                                       //start time measurement
        int index  = Collections.binarySearch(sortedList, number);
        if (index >= 0){
            System.out.println("Бинарный поиск: номер найден, поиск занял " +
                    (System.nanoTime() - methodStart) + "нс");
            return true;
        }
        System.out.println("Бинарный поиск: номер не найден, поиск занял " +
                (System.nanoTime() - methodStart) + "нс");
        return false;
    }

    //search in hashSet with time measurement
    public static boolean searchInHashSet(HashSet<String> hashSet, String number) {
        long methodStart = System.nanoTime();                                           //start time measurement
        if (hashSet.contains(number)){
            System.out.println("Поиск в HashSet: номер найден, поиск занял " +
                    (System.nanoTime() - methodStart) + " нс");
            return true;
        } else {
            System.out.println("Поиск в HashSet: номер не найден, поиск занял " +
                    (System.nanoTime() - methodStart) + " нс");
            return false;
        }
    }

    //search in treeSet with time measurement
    public static boolean searchInTreeSet(TreeSet<String> treeSet, String number) {
        long methodStart = System.nanoTime();                                           //start time measurement
        if (treeSet.contains(number)){
            System.out.println("Поиск в TreeSet: номер найден, поиск занял " +
                    (System.nanoTime() - methodStart) + "нс");
            return true;
        } else {
            System.out.println("Поиск в TreeSet: номер не найден, поиск занял " +
                    (System.nanoTime() - methodStart) + "нс");
            return false;
        }
    }

}
