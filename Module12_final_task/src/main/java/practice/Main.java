package practice;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

public class Main {
    /*
    TODO:
     - реализовать методы класса CoolNumbers
     - посчитать время поиска введимого номера в консоль в каждой из структуры данных
     - проанализоровать полученные данные
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> carNumbers;
        while(true){
            System.out.println("Enter car number:");
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("q")){break;}
            if (CoolNumbers.isCarNumber(input)){
                carNumbers = CoolNumbers.generateCoolNumbers();
                CoolNumbers.bruteForceSearchInList(carNumbers, input);
                CoolNumbers.binarySearchInList(carNumbers, input);
                CoolNumbers.searchInHashSet(new HashSet<>(carNumbers), input);
                CoolNumbers.searchInTreeSet(new TreeSet<>(carNumbers), input);
            }
        }
    }
}
