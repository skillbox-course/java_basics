package org.example;

import org.example.binarySearch.BinarySearch;
import org.example.dataStructures.QueueTest;
import org.example.hash.HashMapTest;
import org.example.linkedLists.*;
import org.example.searchTopElement.SearchTopElement;
import org.example.searchUnique.SearchUnique;
import org.example.sorting.SortTest;

import java.util.Arrays;
import java.util.List;

public class Practice {

    private final int[] randomNonSortedArray = {34, 94, 33, 102, 45, 10, 14};

    public void searchTopElementMethod() {
        System.out.println("====searchTopElementMethod");
        SearchTopElement searchTopElement = new SearchTopElement();
        int[] randomNonSortedArray = {34, 94, 33, 102, 45, 10, 14};
        Arrays.stream(randomNonSortedArray).forEach(elem -> System.out.print(elem + " "));
        int[] topElements = searchTopElement.findTopElements(randomNonSortedArray, 4);
        System.out.print("\ntop 4 elements: ");
        Arrays.stream(topElements).forEach(elem -> System.out.print(elem + " "));
    }

    // O = (N^2)/2
    public void searchUniqueMethod() {
        System.out.println("\n====searchUniqueMethod");
        SearchUnique searchUnique = new SearchUnique();
        int[] testNumbers = {3, 5, 3, 4};
        Arrays.stream(testNumbers).forEach(elem -> System.out.print(elem + " "));
        List<Integer> uniqueNumbers = searchUnique.searchUnique(testNumbers);
        System.out.print("\nunique elements: ");
        uniqueNumbers.forEach(elem -> System.out.print(elem + " "));
    }

    public void searchUniqueFromSortedMethod() {
        System.out.println("\n====searchUniqueFromSortedMethod");
        int[] sortedNumbers = {14, 14, 33, 33, 45, 94, 94, 94, 102};
        Arrays.stream(sortedNumbers).forEach(elem -> System.out.print(elem + " "));
        SearchUnique searchUnique = new SearchUnique();
        List<Integer> uniqueNumbers = searchUnique.searchUniqueFromSorted(sortedNumbers);
        System.out.print("\nunique elements: ");
        uniqueNumbers.forEach(elem -> System.out.print(elem + " "));
    }

    // O = logN
    public void binarySearchMethod() {
        System.out.println("\n====binarySearchMethod");
        int[] sortedNumbers = Arrays.stream(randomNonSortedArray).sorted().toArray();
        BinarySearch binarySearch = new BinarySearch();
        int index = binarySearch.searchNumber(sortedNumbers, 33);
        Arrays.stream(sortedNumbers).forEach(elem -> System.out.print(elem + " "));
        System.out.print("\nindex of 33 in array: " + index);
    }

    public void insertNumberMethod() {
        System.out.println("\n====searchNumberIndexAfter");
        int[] sortedNumbers = Arrays.stream(randomNonSortedArray).sorted().toArray();
        BinarySearch binarySearch = new BinarySearch();
        int number = 94;
        int index = binarySearch.searchNumberIndexAfter(sortedNumbers, number);
        Arrays.stream(sortedNumbers).forEach(elem -> System.out.print(elem + " "));
        System.out.print("\nindex for inserting " + number + ": " + index);
    }

    public void hashMapTestMethod() {
        System.out.println("\n====hashMapTestMethod");
        HashMapTest hashMap = new HashMapTest();
        hashMap.add("key1", "value1");
        hashMap.add("key2", "value2");
        hashMap.add("key3", "value3");
        hashMap.add("key4", "value4");
        hashMap.add("key5", "value5");
        System.out.println(hashMap);

    }

    public void primeNumbersCalculator() {
        System.out.println("====primeNumbersCalculator");
        PrimeNumbersCalculator calculator = new PrimeNumbersCalculator();
        calculator.primeNumbersCalculate(100);
    }

    public void dynamicArrayTest() {
        System.out.println("\n====dynamicArrayTest");
        DynamicArray dynamicArray = new DynamicArray();
        dynamicArray.add(1);
        dynamicArray.add(1);
        dynamicArray.add(1);
        dynamicArray.add(1);
        dynamicArray.remove(3);
        dynamicArray.remove(1);
//        dynamicArray.remove(0);
        System.out.println(dynamicArray);
    }

    public void nodeListTest() {
        System.out.println("\n====nodeListTest");
        NodeList nodeList = new NodeList();
        nodeList.addToEnd(1);
        nodeList.addToEnd(2);
        nodeList.addToEnd(3);
        nodeList.addToEnd(4);
        nodeList.addToEnd(5);
        System.out.println(nodeList);
        System.out.println("reverse");
        nodeList.reverse();
        System.out.println(nodeList);

    }

    public void bracketCheckerMethod() {
        System.out.println("\n====bracketCheckerMethod");
        BracketChecker bracketChecker = new BracketChecker();
        String correct = "a{({})[][()a]}a";
        System.out.print(correct + " ");
        System.out.println(bracketChecker.check(correct));
    }

    public void queueTestMethod() {
        System.out.println("\n====bracketCheckerMethod");
        QueueTest queueTest = new QueueTest(3);
        queueTest.push(1);
        queueTest.push(2);
        queueTest.push(3);
        System.out.println(queueTest);
        System.out.println("first element in queue: " + queueTest.peek());
        System.out.println("delete element: " + queueTest.pop());
        queueTest.push(3);
        System.out.println(queueTest);
        System.out.println("first element in queue: " + queueTest.peek());
    }

    public void sort() {
        System.out.println("\n====sort");
//        int[] array = {5, 6, 7, 8, 9, 4, 3, 2, 1, 0};
        int[] array = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
//        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        SortTest sortTest = new SortTest();
        System.out.println(Arrays.toString(array));
//        sortTest.bubbleSorting(array);
//        sortTest.sortingBySelection(array);
//        sortTest.insertSorting(array);
        sortTest.countingSorting(array);
        System.out.println(Arrays.toString(array));
    }
}