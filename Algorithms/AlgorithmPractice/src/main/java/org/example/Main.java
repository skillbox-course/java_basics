package org.example;

public class Main {
    public static void main(String[] args) {
        Practice practice = new Practice();
        practice.searchTopElementMethod();
        practice.searchUniqueMethod();
        practice.searchUniqueFromSortedMethod();
        practice.binarySearchMethod();
        practice.insertNumberMethod();
        practice.hashMapTestMethod();
        practice.primeNumbersCalculator();
        practice.dynamicArrayTest();
        practice.nodeListTest();
        practice.bracketCheckerMethod();
        practice.queueTestMethod();
        practice.sort();

    }
}
