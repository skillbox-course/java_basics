package org.example.searchTopElement;

public class SearchTopElement {

    private int findMaxUnderBoundary(int[] inputArray, int topBoundary) {
        int currentMax = Integer.MIN_VALUE;
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < topBoundary) {
                currentMax = Math.max(inputArray[i], currentMax);
            }
        }
        return currentMax;
    }

    public int[] findTopElements(int[] inputArray, int numberOfElements) {
        int[] topElements = new int[numberOfElements];
        int previousMax = Integer.MAX_VALUE;

        for (int i = 0; i < numberOfElements; i++) {
            int currentMax = findMaxUnderBoundary(inputArray, previousMax);
            previousMax = currentMax;
            topElements[i] = currentMax;
        }
        return topElements;
    }

}
