package org.example.dataStructures;

import java.util.Arrays;

public class QueueTest {

    int[] data;
    int begin;
    int end;
    int counter;

    public QueueTest(int maxSize) {
        this.data = new int[maxSize];
        begin = 0;
    }

    public boolean isEmpty() {
        return counter == 0;
    }

    public boolean isFull() {
        return counter == data.length;
    }

    public void push(int value) {
        if (isFull()) {
//            resize();
            System.out.println("The queue is full");
            return;
        }
        data[end] = value;
        counter++;
        if (end != data.length -1) {
            end++;
        } else {
            end = 0;
        }
    }

    private void resize() {
        int[] newQueue = new int[data.length * 2];
        int index = 0;
        for (int j = begin; j < end; j++) {
            newQueue[index] = data[j];
            index++;
        }
        data = newQueue;
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("The queue is empty");
            return 0;
        }
        int value = data[begin];
        begin++;
        if (begin == data.length) {
            begin = 0;
        }
        counter--;
        return value;
    }

    public int peek() {
        return data[begin];
    }

    @Override
    public String toString() {
        return Arrays.toString(data) +
                ", beginIndex = " + begin +
                ", count of elements = " + counter;
    }
}
