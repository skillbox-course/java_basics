package org.example.linkedLists;

public class DynamicArray {
    private int[] array;
    private int countOfElements;

    public DynamicArray() {
        this.array = new int[1];
        this.countOfElements = 0;
    }

    public void set(int index, int value) {
        this.array[index] = value;
    }

    public int get(int index) {
        return array[index];
    }

    public void add(int value) {
        if (countOfElements == array.length) {
            int[] newArray = new int[array.length * 2];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
        array[countOfElements] = value;
        countOfElements++;
    }

    public void remove(int index) {
        if (index > countOfElements) return;
        countOfElements--;
        boolean needToReduce = countOfElements * 4 <= array.length;
        int[] newArray = new int[needToReduce ? countOfElements / 4 : countOfElements];
        for (int i = 0; i <= countOfElements; i++) {
            if (i < index) newArray[i] = array[i];
            if (i > index) newArray[i - 1] = array[i];
        }
        array = newArray;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int element : array) {
            stringBuilder.append(element).append("; ");
        }
        String result = stringBuilder.toString();
        if (result.length() > 2) {
            return result.substring(0, result.length() - 2);
        }
        return result;
    }
}


















