package org.example.linkedLists;

import java.util.Iterator;

public class NodeList implements Iterable<Node> {
    public Node begin;
    public Node end;

    public boolean isEmpty() {
        return begin == null;
    }

    public void addToStart(int value) {
        Node node = new Node(value);
        if (isEmpty()) {
            begin = node;
            end = node;
            return;
        }
        node.next = begin;
        begin.prev = node;
        begin = node;
    }

    public void insertAfter(Node prevNode, int value) {
        Node node = new Node(value);
        if (prevNode == end) {
            node.prev = prevNode;
            prevNode.next = node;
            end = node;
            return;
        }
        Node nextNode = prevNode.next;
        node.next = nextNode;
        node.prev = prevNode;
        prevNode.next = node;
        nextNode.prev = node;
    }

    public void addToEnd(int value) {
        if (end == null) {
            Node node = new Node(value);
            begin = node;
            end = node;
            return;
        }
        insertAfter(end, value);
    }

    public void delete(Node node) {
        Node prevNode = node.prev;
        Node nextNode = node.next;
        if (begin == node) {
            begin = nextNode;
            nextNode.prev = null;
            return;
        }
        if (end == node) {
            end = prevNode;
            prevNode.next = null;
            return;
        }
        prevNode.next = nextNode;
        nextNode.prev = prevNode;
    }

    private void revertElem(Node node) {
        if (node != null) {
            Node tmpNode = node.next;
            node.next = node.prev;
            node.prev = tmpNode;
            if (node.prev == null) {
                begin = node;
            }
            if (node.next == null) {
                end = node;
            }
            revertElem(tmpNode);
        }
    }

    public void reverse() {
        revertElem(begin);
    }

    @Override
    public Iterator<Node> iterator() {
        return new Iterator() {
            Node pointer = begin;

            @Override
            public boolean hasNext() {
                return pointer != null;
            }

            @Override
            public Node next() {
                Node node = pointer;
                pointer = pointer.next;
                return node;
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Node node : this) {
            stringBuilder.append(node).append("; ");
        }
        String result = stringBuilder.toString();
        return result == null ? result : result.substring(0, result.length() - 2);
    }
}
