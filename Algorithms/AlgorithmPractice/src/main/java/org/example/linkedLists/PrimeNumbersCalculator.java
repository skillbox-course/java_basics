package org.example.linkedLists;

public class PrimeNumbersCalculator {

    public void primeNumbersCalculate(int number){

        for (int i = 1; i < number; i++) {
            boolean isPrime = true;
            // O(N*(N+1)/2)
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) System.out.print(i + "; ");
        }
    }
}

