package org.example.linkedLists;

import java.util.Stack;

public class BracketChecker {
    public boolean check(String string) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '{') stack.push('{');
            if (string.charAt(i) == '(') stack.push('(');
            if (string.charAt(i) == '[') stack.push('[');
            if (stack.empty() &&
                    (string.charAt(i) == '}' || string.charAt(i) == ')' || string.charAt(i) == ']')) {
                return false;
            }
            if (string.charAt(i) == '}' && stack.peek().equals('{')) {
                stack.pop();
                continue;
            }
            if (string.charAt(i) == ')' && stack.peek().equals('(')) {
                stack.pop();
                continue;
            }
            if (string.charAt(i) == ']' && stack.peek().equals('[')) {
                stack.pop();
            }
        }
        return stack.isEmpty();
    }
}
