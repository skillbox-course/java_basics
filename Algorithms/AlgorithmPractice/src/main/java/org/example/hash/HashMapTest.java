package org.example.hash;

public class HashMapTest {
    class KeyValuePair {
        public String key;
        public String value;

        public KeyValuePair(String key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return key + " - " + value;
        }
    }

    // TODO:
    int hashFunction(String key) {
        return key.hashCode();
    }

    private int size = 4;
    private KeyValuePair[] entries = new KeyValuePair[size];
    private int currentEntry = 0;

    public void add(String key, String value) {
        int index = findCorrectIndex(key);
        entries[index] = new KeyValuePair(key, value);
        currentEntry++;
        if (currentEntry == size) {
            resize(size * 2);
        }
    }

    private void resize(int newSize) {
        KeyValuePair[] newEntries = new KeyValuePair[newSize];
        for (int i = 0; i < size; i++) {
            KeyValuePair entry = entries[i];
            int index = findCorrectIndex(entry.key);
            newEntries[index] = entry;
        }
        size = newSize;
        entries = newEntries;
    }

    public String getValue(String key) {
        int index = findCorrectIndex(key);
        return entries[index].value;
    }

    private int findCorrectIndex(String key) {
        int hash = hashFunction(key);
        int index = hash % size;

        for (int i = 0; i < size; i++) {
            int testIndex = (index + i) % size;
            KeyValuePair entry = entries[testIndex];
            if (entry == null || entry.key.equals(key)){
                return testIndex;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            stringBuilder.append(entries[i]).append("\n");
        }

        return stringBuilder.toString();
    }
}
