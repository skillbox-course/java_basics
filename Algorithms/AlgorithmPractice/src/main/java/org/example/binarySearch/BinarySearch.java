package org.example.binarySearch;

public class BinarySearch {

    public int searchNumber(int[] testArray, int number) {
        int left = 0;
        int right = testArray.length-1;
        while (left <= right) {
            int middle = (left + right) / 2;

            if (number < testArray[middle]) {
                right = middle - 1;
            }
            if (number > testArray[middle]) {
                left = middle + 1;
            }
            if (number == testArray[middle]) {
                return middle;
            }
        }
        return -1;
    }

    // if number = arr[i], insert before arr[i] into arr[i-1]
    public int searchNumberIndexBefore(int[] testArray, int number) {
        int left = 0;
        int right = testArray.length;

        while (left < right) {
            int middle = (left + right) / 2;
            int m = testArray[middle];
            if (m < number) {
                left = middle +1;
            } else {
                right =middle;
            }
        }
        return left;
    }

    // if number = arr[i], insert after arr[i] into arr[i+1]
    public int searchNumberIndexAfter(int[] testArray, int number) {
        int left = 0;
        int right = testArray.length;

        while (right > left) {
            int middle = (left + right) / 2;
            int m = testArray[middle];
            if (number >= m) {
                left = middle +1;
            }
            if (number < m) {
                right = middle -1;
            }
        }
        return left;
    }

}
