package org.example.searchUnique;

import java.util.ArrayList;
import java.util.List;

public class SearchUnique {

    public List<Integer> searchUnique(int[] testNumbers) {
        List<Integer> uniqueNumbers = new ArrayList<>();

        for (int currentNumber : testNumbers) {
            boolean addedToUniqueNumbers = false;
            for (int uniqueNumber : uniqueNumbers) {
                if (uniqueNumber == currentNumber) {
                    addedToUniqueNumbers = true;
                    break;
                }
            }
            if (!addedToUniqueNumbers) {
                uniqueNumbers.add(currentNumber);
            }
        }
        return uniqueNumbers;
    }

    public List<Integer> searchUniqueFromSorted(int[] sortedNumbers) {
        List<Integer> uniqueNumbers = new ArrayList<>();
        int previousNumber = sortedNumbers[0];
        for (int i = 0; i < sortedNumbers.length; i++) {
            if (previousNumber != sortedNumbers[i]) {
                uniqueNumbers.add(previousNumber);
                previousNumber = sortedNumbers[i];
            }
        }
        uniqueNumbers.add(previousNumber);
        return uniqueNumbers;
    }
}
