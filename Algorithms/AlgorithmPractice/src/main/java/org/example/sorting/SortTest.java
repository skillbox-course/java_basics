package org.example.sorting;

public class SortTest {

    private boolean isSorted;

    public void bubbleSorting(int[] array) {
        for (int i = 0; i < array.length; i++) {
            isSorted = false;
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int max = array[i];
                    array[i] = array[j];
                    array[j] = max;
                    isSorted = true;
                }
            }
            if (!isSorted) {
                break;
            }
        }
    }

    public void sortingBySelection(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int x = array[i];
            array[i] = array[minIndex];
            array[minIndex] = x;
        }
    }

    public void insertSorting(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int x = array[i];
            int xIndex;
            for (xIndex = i; (xIndex > 0 && (array[xIndex - 1] > x)); xIndex--) {
                array[xIndex] = array[xIndex - 1];
            }
            array[xIndex] = x;
        }
    }

    public void countingSorting(int[] array) {
        int[] newArray = new int[1000];
        for (int i = 0; i < array.length; i++) {
            newArray[array[i]] += 1;
        }
        int index= 0;
        for (int i = 0; i < newArray.length; i++) {
            for (int j = 0; j < newArray[i]; j++) {
                array[index] = i;
                index++;
            }
        }
    }
}
