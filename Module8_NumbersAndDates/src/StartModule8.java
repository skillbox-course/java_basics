import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

public class StartModule8 {
    public static void main(String[] args) {
        LocalDateTime nowDateTime = LocalDateTime.now();

        //converting DateTime to seconds and back
        LocalDateTime dateTime = LocalDateTime.
                ofEpochSecond(System.currentTimeMillis()/1000,0, ZoneOffset.ofHours(-3));
        System.out.println("\n"+dateTime);
        System.out.println(nowDateTime.toEpochSecond(ZoneOffset.ofHours(-5)));

        //calculating difference between two dates
        DateCalculator dateCalculator = new DateCalculator();
        System.out.println("\n"+dateCalculator.getPeriodFromJavaBirthday());


        //date comparison
        LocalDateTime someSecondDate = nowDateTime.plusHours(3);
        System.out.println("\n"+nowDateTime.isBefore(someSecondDate));
        System.out.println(nowDateTime.compareTo(someSecondDate));
        System.out.println(nowDateTime.until(someSecondDate, ChronoUnit.MINUTES));

        //LocalDateTime + Formatter
        DateTimeFormatter formatter =DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
        System.out.println("\n"+formatter.format(nowDateTime));
        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                .localizedBy(new Locale("nl"));
        System.out.println(formatter.format(nowDateTime));

        //Formatter
        String someStringDateTime = "06;2037;13";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM;yyyy;dd");
        System.out.println("\n"+LocalDate.parse(someStringDateTime, dateTimeFormatter));

        //localeDate
        LocalDate nowLocalDate = LocalDate.now();
        System.out.println("\n"+nowLocalDate);
        LocalDate specificDate = LocalDate.of(2000, 5,28);
        System.out.println(specificDate.minusWeeks(4));
        System.out.println(LocalDateTime.now(ZoneId.of("Europe/Amsterdam")));

        //date
        Date date = new Date();
        System.out.println("\n"+date);


        MoneyCalculator moneyCalculator = new MoneyCalculator();
        //how many cassettes needed to pack 1999 banknotes
        System.out.print("\n"+moneyCalculator.calculateCassetteCount(1999));
        System.out.print(" "+moneyCalculator.calculateCassetteCount(2000));
        System.out.print(" "+moneyCalculator.calculateCassetteCount(2001));
        System.out.print(" "+moneyCalculator.calculateCassetteCount(2500));
        System.out.print(" "+moneyCalculator.calculateCassetteCount(4000));
        System.out.print(" "+moneyCalculator.calculateCassetteCount(4001));
    }
}
