import java.time.LocalDate;
import java.time.Period;


public class DateCalculator {
    public static final LocalDate JAVA_BIRTHDAY = LocalDate.of(1995,5,23);

    //method for calculating the difference between two dates in custom format
    public String getPeriodFromJavaBirthday(){
        LocalDate now = LocalDate.now();
        Period period = DateCalculator.JAVA_BIRTHDAY.until(now);
        return period.getYears() +"years, " + period.getMonths()+"month, " + period.getDays()+"days";
    }
}