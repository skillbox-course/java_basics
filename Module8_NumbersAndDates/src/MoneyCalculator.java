//class for calculate how many cassette needed for packs all banknotes
public class MoneyCalculator {
    //number of banknotes in one package
    private static final int BANKNOTES_PER_PACK = 100;
    //number of packs in one cassette
    private static final int PACKS_IN_CASSETTE = 20;
    public int calculateCassetteCount(int banknoteCount){
        //calculate how many packs needed
        int packsCount = banknoteCount / BANKNOTES_PER_PACK +
                (banknoteCount % BANKNOTES_PER_PACK == 0 ? 0 : 1);

        //calculate how many cassette needed
        return packsCount / PACKS_IN_CASSETTE +
                (packsCount % PACKS_IN_CASSETTE == 0 ? 0 : 1);
    }
}
