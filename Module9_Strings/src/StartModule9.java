public class StartModule9 {
    public static void main(String[] args) {
        String testString;
        StringExperiments stringExperiments = new StringExperiments();

        //extracting a quotes from a string
        testString = "Дмитрий сообщил следующее: "
                .concat("«Я вернусь в 12:40 и, будьте добры, подготовьте к этому времени все документы!»")
                .concat("На что Анна ему ответила: «А документы-то так и не привезли». ")
                .concat("Дмитрий удивлённо посмотрел на неё и сказал: ")
                .concat("«Ну и ладно», — вздохнул, махнул рукой и удалился.");
        stringExperiments.extractQuote(testString);

        //extracting a link to a website from a string
        testString = "Алексей, добрый день!\nМой гитхаб — https://github.com/,"
                .concat("а также ссылка на мой персональный сайт — https://www.skillbox.ru/")
                .concat("\nЕсли возникнут вопросы, пишите мне напрямую. Я всегда доступен");
        stringExperiments.extractLink(testString);
        stringExperiments.isCarNumberCorrect("A234ВР987");
        testString = "uncle Bob  phone: +7(911)123-4567";
        stringExperiments.splittingString(testString);
        System.out.println("just number " + stringExperiments.extractPhoneNumber(testString));

        //searching for numbers in a string
        testString = "\nВася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";
        System.out.println("total salary: "+stringExperiments.calculateSalary(testString));

        //splitting string
        System.out.println(stringExperiments.splittingString("Беллинсгаузен Васисуалий Никифорович"));
        //combining strings
        System.out.println(stringExperiments.combiningString());

        //searching for encoding substring in string
        System.out.println(stringExperiments.substringSearch("\nContent-Type: text/html; charset=UTF-8;"));
        System.out.println(stringExperiments.substringSearch("Content-Type: text/html; charset=Windows-1251;"));
        System.out.println(stringExperiments.substringSearch("Content-Type: text/html;"));
        System.out.println(stringExperiments.substringSearch("Content-Type: text/html; charset="));
        System.out.println(stringExperiments.substringSearch("Content-Type: text/html; charset=;"));

        // work with string symbols
        testString = "7просто 11 текст 935 for testing 62  ";
        stringExperiments.workWithStringsSymbols(testString);

        //convert numbers to string
        double simpleNumber = 3.28;
        Double.toString(simpleNumber);
        Double.valueOf(simpleNumber).toString();
        String.valueOf(simpleNumber);
        testString = "35473679";
        int simpleIntNumber = Integer.parseInt(testString);
        stringExperiments.simpleCalculation();

        //string comparison using StringExperiments class
        stringExperiments.stringComparison();

        //testing speed of string concatenation in different ways
        testString = "\nvariable i = ";
        System.out.println("string concatenation with stringBuilder: " +
                stringExperiments.speedTestStingConcatenationWithAssignment(testString));
        System.out.println("string concatenation with stringBuilder: "
                + stringExperiments.speedTestStingConcatenationWithStringBuilder(testString));
        System.out.println("string concatenation with string concat method: "
                + stringExperiments.speedTestStingConcatenationWithStringConcat(testString));

        //comparison two lines
        testString = "test string";
        String testString2 = "test ".concat("string");
        System.out.println("comparison two lines \n" + "using == " + (testString == testString2)
                        + " using equal method " + testString.equals(testString2));

        //checking the string for emptiness
        testString = "";
        System.out.println("\nIs the string empty: " + (testString.length() == 0));
        System.out.println("Is the string empty: " + testString.isEmpty());
        testString = "\t   \n";
        System.out.println("Is the string blank: " + testString.isBlank());
    }
}