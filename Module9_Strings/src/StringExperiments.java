import org.w3c.dom.ls.LSOutput;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringExperiments {

    //extracting a quotes from a string
    public void extractQuote(String testString){
        System.out.println(testString);
        String quote = "«([^»]+)»";
        Pattern pattern = Pattern.compile(quote);
        Matcher matcher = pattern.matcher(testString);
        while(matcher.find()){
            System.out.println(matcher.group(1));
        }
    }

    //extracting a link to a website from a string
    public void extractLink(String testString){
        System.out.println(testString);
        String linkRegex = "https://[^\\s,]+";
        Pattern pattern = Pattern.compile(linkRegex);
        Matcher matcher = pattern.matcher(testString);
        while(matcher.find()){
            int start = matcher.start();
            int end = matcher.end();
//            System.out.println(testString.substring(start, end));
            System.out.println(matcher.group());
        }
    }

    //checking car number line for compliance string regex
    public boolean isCarNumberCorrect(String carNumber){
        String lettersList = "[ABEKMHOPCTYXАВЕКМНОРСТУК]";
        String carNumberRegex = lettersList + "[0-9]{3}" + lettersList + "{2}[0-9]{2,3}";
        System.out.println("is car number correct? "+carNumber.matches(carNumberRegex));
        return carNumber.matches(carNumberRegex);
    }

    //splitting string to words
    public void splitStringToWords(String testString){
        System.out.println(testString);
        String[] words = testString.split("\s+");
        for (int i = 0; i < words.length; i++){
            System.out.println(words[i]);
        }
    }

    //extracting a phone number
    public String extractPhoneNumber(String testString){
        String phoneRegex = "[^0-9]";
        return testString.replaceAll(phoneRegex,"");
    }

    //searching for numbers in a string
    public int calculateSalary(String testString){
        System.out.println(testString);
        int totalSalary = 0;
        for (int i = 0; i < testString.length(); i++){
            if (Character.isDigit(testString.charAt(i))) {
                totalSalary += Integer.parseInt(testString.substring(i,testString.indexOf(' ',i)));
                i = testString.indexOf(' ',i);
            }
        }
        return totalSalary;
    }

    //splitting string
    public String splittingString(String testString){
        int firstSpace = testString.indexOf(' ');
        int lastSpace = testString.lastIndexOf(' ');
        String surname = testString.substring(0,firstSpace);
        String name = testString.substring(firstSpace,lastSpace);
        String patronymic = testString.substring(lastSpace);
        String template = "%nsurname: %s%nname: %s%npatronymic%s";
        return String.format(template,surname,name,patronymic);
    }

    //combining strings
    public String combiningString(){
        String name1 = "Oleg";
        String name2 = "Maria";
        String name3 = "Pavel";
        String names = String.join(", ",name1,name2,name3);
        String names2 = String.join(", ",name1,name2,name3);
        String template = "names: %s";
        String result = String.format(template,names,names2);
        return result;
    }

    //searching for encoding substring in string
    public String substringSearch(String testString){
        System.out.println(testString);
        int start = testString.indexOf("charset=");
        int end = testString.indexOf(';', start);
        if (start < 0 || end < 0 || end - (start+"charset=".length()) == 0){
            return "substring not found";
        }
        return testString.substring(start+"charset=".length(),end);
    }

    public void workWithStringsSymbols(String testString){
        //output testString in different encodings
        System.out.println("\n"+Charset.defaultCharset());
        /*UTF-8*/
        byte[] testStringBytes = testString.getBytes(StandardCharsets.UTF_8);
        for (byte tsByte : testStringBytes) System.out.print(tsByte + " ");
        System.out.println("\nWindows-1251");
        /*Windows-1251*/
        try {
            testStringBytes = testString.getBytes("Windows-1251");
            for (byte tsByte : testStringBytes) System.out.print(tsByte + " ");
        } catch (UnsupportedEncodingException e){
            throw new RuntimeException();
        }
        /*US_ASCII*/
        String testStringUS_ASCII = new String(testString.getBytes(), StandardCharsets.US_ASCII);
        System.out.println("\nUS_ASCII "+testStringUS_ASCII);

        //character-by-character string output
        System.out.println();
        char[] chars = testString.toCharArray();
        for (char ch : chars){
            System.out.print(ch);
        }
        System.out.println();
        //output of string characters in reverse order
        for (int i = testString.length() - 1; i >=0; i-- ){
            System.out.print(testString.charAt(i));
        }
        //counting spaces in string
        int spaceCount = 0;
        for (char ch : chars){
            if (ch == ' ') spaceCount++;
        }
        System.out.println("\nspaces in the string: "+spaceCount);
        //removing spaces at the end of string
        System.out.println("test string: " + testString);
        System.out.println("test string, method trim: " + testString.trim());
        System.out.println("test string, method strip: " + testString.strip());

        //counting digits in string
        int digitCount = 0;
        for (int i = 0; i < testString.length(); i++){
            if(Character.isDigit(testString.charAt(i))) {
                digitCount++;
            }
        }
        System.out.println("digits in the string: "+digitCount);
    }

    //enter number then math operation (+ - * /) and then second number. The result will be displayed
    public void simpleCalculation() {
        float result = 0;
        System.out.println("Enter first number:");
        int firstNumber = Integer.parseInt(new Scanner(System.in).nextLine());
        System.out.println("Enter math operation:");
        String mathOperation = new Scanner(System.in).nextLine();
        System.out.println("Enter second number:");
        int secondNumber = Integer.parseInt(new Scanner(System.in).nextLine());
        switch (mathOperation){
            case ("+"): result = Math.addExact(firstNumber,secondNumber); break;
            case ("-"): result = Math.subtractExact(firstNumber,secondNumber); break;
            case ("*"): result = Math.multiplyExact(firstNumber,secondNumber); break;
            case ("/"): result = firstNumber / (float)secondNumber ; break;
        }
        System.out.println(result);
    }

    //enter two numbers and the result of their comparison will be displayed
    public void stringComparison() {
        while (true) {
            String result = "";
            System.out.println("to exit, enter q\nEnter first line:");
            String firstString = new Scanner(System.in).nextLine();
            if (firstString.equals("q")) break;
            System.out.println("Enter second line:");
            String secondString = new Scanner(System.in).nextLine();

            String isLinesEqualSymbol = firstString.equals(secondString) ? " = " : " != ";
            result = result.concat(firstString).concat(isLinesEqualSymbol).concat(secondString);
            System.out.println(result);
        }
    }

    public long speedTestStingConcatenationWithAssignment(String testString) {
        long loopStartTime = System.currentTimeMillis();
        for (int i = 0; i <= 10_000; i++) {
            testString += "\nvariable i = ";
        }
        return System.currentTimeMillis() - loopStartTime;
    }

    public long speedTestStingConcatenationWithStringBuilder(String testString) {
        StringBuilder testStringBuilder = new StringBuilder();
        long loopStartTime = System.currentTimeMillis();
        for (int i = 0; i <= 10_000; i++) {
            testStringBuilder.append(testString);
        }
        return System.currentTimeMillis() - loopStartTime;
    }

    public long speedTestStingConcatenationWithStringConcat(String testString) {
        long loopStartTime = System.currentTimeMillis();
        for (int i = 0; i <= 20_000; i++) {
            testString.concat(testString);
        }
        return System.currentTimeMillis() - loopStartTime;
    }
}
