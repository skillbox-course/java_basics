import java.util.HashSet;
import java.util.TreeSet;

public class StarModule11 {
    public static void main(String[] args) {
        TreeSetExperiments treeSetExperiments = new TreeSetExperiments();
        HashSet<Order> ordersManager = new HashSet<>();
        HashSet<Order2> ordersManager2 = new HashSet<>();

        //testing TreeSet class methods
        treeSetExperiments.treeSetMethods(TreeSetExperiments.treeSetTodoList);
        System.out.println();

        //testing equals speed. Class order has standard hashcode method
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1_000_000; i++){
            Order order = new Order("test order" + (int)(1000 * Math.random()));
            ordersManager.add(order);
        }
        System.out.println("time of loop with orders: " + (System.currentTimeMillis() - start));
        System.out.println("equals count: " + Order.getEqualsCount());
        System.out.println(ordersManager.size());
        ordersManager.clear();

        //testing equals speed. Class order2 - hashcode method returns 1
        long start2 = System.currentTimeMillis();
        for (int i = 0; i < 1_000_000; i++){
            Order2 order =
                    new Order2("test order" + (int)(1000 * Math.random()));
            ordersManager2.add(order);
        }
        System.out.println("time of loop with orders with hashcode = 1: " + (System.currentTimeMillis() - start2));
        System.out.println("equals count: " + Order2.getEqualsCount());
        System.out.println(ordersManager2.size());

        //checking uniqueness treeSet. Compare order classes using Comparator class
        TreeSet<Order> ordersManagerTreeSet = new TreeSet<>(new OrderComparator());
        ordersManagerTreeSet.add(new Order("milk"));
        ordersManagerTreeSet.add(new Order("cat"));
        ordersManagerTreeSet.add(new Order("vacuum cleaner"));
        ordersManagerTreeSet.add(new Order("teapot"));
        ordersManagerTreeSet.add(new Order("milk"));
        ordersManagerTreeSet.add(new Order("CAT"));
        System.out.println(ordersManagerTreeSet);

        //checking uniqueness treeSet. Compare order classes using Comparable interface
        TreeSet<Order2> ordersManagerTreeSet2 = new TreeSet<>();
        ordersManagerTreeSet2.add(new Order2("milk"));
        ordersManagerTreeSet2.add(new Order2("cat"));
        ordersManagerTreeSet2.add(new Order2("vacuum cleaner"));
        ordersManagerTreeSet2.add(new Order2("teapot"));
        ordersManagerTreeSet2.add(new Order2("milk"));
        ordersManagerTreeSet2.add(new Order2("CAT"));
        System.out.println(ordersManagerTreeSet2);

        //test class SetExperiments for practice with Set collection. Using some methods of the Set class
        SetExperiments setExperiments =new SetExperiments();
        setExperiments.fillingTodoList();
        System.out.println("\n" + setExperiments.getTodoList());
        //using the contains method of Set collection class
        System.out.println("is the tasks set contain \"take out the trash\"? "
                + setExperiments.getTodoList().contains("take out the trash"));
        System.out.println("size of the tasks set: " + setExperiments.getTodoList().size());
        setExperiments.getTodoList().clear();
        System.out.println("clear the tasks set");
        System.out.println("is the tasks set empty? " + setExperiments.getTodoList().isEmpty());
    }
}