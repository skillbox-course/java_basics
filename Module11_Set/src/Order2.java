import java.time.LocalDateTime;
import java.util.Objects;

//hashcode = 1 for testing
public class Order2 implements Comparable<Order2> {
    private String order;
    private final LocalDateTime time;
    private boolean isDone;
    private static int equalsCount;

    @Override
    public int compareTo(Order2 order) {
        return (this.getTime().compareTo(order.getTime()) != 0) ?
                this.getTime().compareTo(order.getTime()) : this.getOrder().compareTo(order.getOrder());
    }

    public Order2(String order) {
        this.order = order;
        time = LocalDateTime.now();
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public static int getEqualsCount() {
        return equalsCount;
    }

    @Override
    public boolean equals(Object o) {
        equalsCount++;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order2 that = (Order2) o;
        return isDone == that.isDone && Objects.equals(order, that.order);
    }

    @Override
    public int hashCode() {
        return 1; // Objects.hash(order, isDone);
    }

    @Override
    public String toString() {
        return "\nOrder='" + order + '\'' +
                ", time=" + time +
                ", isDone=" + isDone +
                '}';
    }

}
