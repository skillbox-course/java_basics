import java.time.LocalDateTime;
import java.util.Objects;

public class Order {
    private String order;
    private final LocalDateTime time;
    private boolean isDone;
    private static int equalsCount;

    public Order(String order) {
        this.order = order;
        time = LocalDateTime.now();
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public static int getEqualsCount() {
        return equalsCount;
    }

    @Override
    public boolean equals(Object o) {
        equalsCount++;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order1 = (Order) o;
        return isDone == order1.isDone && Objects.equals(order.toLowerCase(), order1.order.toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(order.toLowerCase(), isDone);
    }

    @Override
    public String toString() {
        return "\nOrder='" + order + '\'' +
                ", time=" + time +
                ", isDone=" + isDone +
                '}';
    }
}
