import java.util.Comparator;

public class OrderComparator implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return (order1.getTime().compareTo(order2.getTime()) != 0) ?
                order1.getTime().compareTo(order2.getTime()) : order1.getOrder().compareTo(order2.getOrder());
    }
}
