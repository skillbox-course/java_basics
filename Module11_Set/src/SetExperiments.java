import java.util.HashSet;

public class SetExperiments {
    private HashSet <String> todoList = new HashSet<>();
    public void fillingTodoList(){
        todoList.add("buy milk");
        todoList.add("feed a cat");
        todoList.add("take out the trash");
        todoList.add("buy milk");
        todoList.add("feed a cat");
    }

    public HashSet<String> getTodoList() {
        return todoList;
    }

    public void setTodoList(HashSet<String> todoList) {
        this.todoList = todoList;
    }
}
