import java.util.TreeSet;

public class TreeSetExperiments {
    public static TreeSet<String> treeSetTodoList = new TreeSet<>();
    public static void fillingTodoList(){
        treeSetTodoList.add("10");
        treeSetTodoList.add("20");
        treeSetTodoList.add("30");
        treeSetTodoList.add("40");
        treeSetTodoList.add("50");
    }

    public TreeSetExperiments() {
        fillingTodoList();
    }

    public void treeSetMethods(TreeSet<String> treeSetCollection){
        System.out.println(treeSetCollection);
        //the method finds the element of collection greater than or equal to transmitted parameter
        System.out.println(
                "ceiling: the method finds the element of collection greater than or equal to transmitted parameter: "
                        + treeSetCollection.ceiling("32"));
        //the method finds the nearest element of collection lest than or equal to transmitted parameter
        System.out.println(
                "floor: the method finds the nearest element of collection lest than or equal to transmitted parameter: "
                        + treeSetCollection.floor("32"));
        //the method finds an exact match and returns boolean
        System.out.println("contains: is treeSetCollection contain \"32\"? " + treeSetCollection.contains("32"));
        //the method CUT first element of collection (!return and delete the element)
        System.out.println(
                "pollFirst: the method CUT first element of collection (!return and delete the element): "
                + treeSetCollection.pollFirst());
        treeSetTodoList.add("10");
        System.out.println(
                "higher: the method returns the least element greater than e: "
                        + treeSetCollection.higher("32")
                        + "\nlower: the method returns the least element greater than e: "
                        + treeSetCollection.lower("32"));
        System.out.println("subset: returns subset from element to element\n(example 18 - 42): "
                + treeSetTodoList.subSet("18", true, "42", true));
    }
}
