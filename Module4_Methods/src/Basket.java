public class Basket {
    private static int basketCount = 0;
    private static double allBasketTotalPrice;
    private static int allItemsTotalCount;
    private String items = "";
    private double totalPrice = 0;
    private int limit;
    private double totalWeight = 0;

    public Basket() {
        increaseBasketCount(1);
        items = "Список товаров:";
        this.limit = 1000000;
    }

    public Basket(int limit) {
        this();
        this.limit = limit;
    }

    public Basket(String items, int totalPrice) {
        this();
        this.items = this.items + items;
        this.totalPrice = totalPrice;
        increaseAllBasketTotalPrice(totalPrice);
        increaseAllItemsTotalCount(1);
    }

    public static double getAllBasketTotalPrice(){
        return allBasketTotalPrice;
    }
    public static int getAllItemsTotalCount(){
        return allItemsTotalCount;
    }
    public static void increaseAllBasketTotalPrice(double price){
        Basket.allBasketTotalPrice = Basket.allBasketTotalPrice + price;
    }
    public static void increaseAllItemsTotalCount(int count){
        Basket.allItemsTotalCount = Basket.allItemsTotalCount + count;
    }
    public static double allItemsAveragePrice(){
        return allBasketTotalPrice / allItemsTotalCount;
    }
    public static double allBasketAveragePrice(){
        return allBasketTotalPrice / basketCount;
    }

    public static int getBasketCount() {
        return basketCount;
    }

    public static void increaseBasketCount(int count) {
        Basket.basketCount = Basket.basketCount + count;
    }

    public void add(String name, int price) {
        add(name, price, 1,0);
    }

    public void add(String name, int price, int count) {
        add(name, price,count,0);
    }
    public void add(String name, double price, int count, double weight) {
        boolean error = false;
        if (contains(name)) {
            error = true;
        }

        if (totalPrice + count * price >= limit) {
            error = true;
        }

        if (error) {
            System.out.println("Error occurred :(");
            return;
        }

        items = items + "\n" + name + " - " +
                count + " шт. - " + price;
        totalPrice = totalPrice + (count * weight) * price;
        totalWeight = totalWeight + count * weight;
        increaseAllBasketTotalPrice((count * weight) * price);
        increaseAllItemsTotalCount(count);
    }

    public void clear() {
        items = "";
        totalPrice = 0;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public double getTotalWeight(){
        return totalWeight;
    }

    public boolean contains(String name) {
        return items.contains(name);
    }

    public void print(String title) {
        System.out.println(title);
        if (items.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            System.out.println(items + "\n" + "total price " + getTotalPrice() + " total weight " + getTotalWeight());
        }
    }
}
