public class Arithmetic {
    private int a = 0;
    private int b = 0;

    public void summa() {
        int sum = a+b;
        System.out.println("a+b="+sum);
    }

    public void multiplication() {
        long arithmeticProduct = a*b;
        System.out.println("a*b="+arithmeticProduct);
    }

    public void maxNumber() {
        if (a > b) {
            System.out.println("max number = " + a);
        }else if (b > a) {
            System.out.println("max number = " + b);
        } else {
            System.out.println("a is equal to b = " + a);
        }
    }
    public void minNumber() {
        if (a < b) {
            System.out.println("min number = " + a);
        }else if (b < a) {
            System.out.println("min number = " + b);
        } else {
            System.out.println("a is equal to b = " + a);
        }
    }

    public Arithmetic(int a, int b) {
        this.a = a;
        this.b = b;
    }
}
