public class Main {
    public static void main(String[] args) {

        Arithmetic arithmetic = new Arithmetic(5,6);
        arithmetic.summa();
        arithmetic.multiplication();
        arithmetic.maxNumber();
        arithmetic.minNumber();

        Basket basket = new Basket();
        basket.add("Milk", 40);
        basket.add("Meat",150, 2,1.72);
        basket.print("Milk");

        Basket basket2 = new Basket("bread",75);


        System.out.println("\nBasket count: "+Basket.getBasketCount()+
                            "\ntotal item count: "+Basket.getAllItemsTotalCount()+
                            "\nall baskets total price: "+Basket.getAllBasketTotalPrice()+
                            "\nbasket average price: "+Basket.allBasketAveragePrice()+
                            "\nitems average price: "+Basket.allItemsAveragePrice());
    }
}
