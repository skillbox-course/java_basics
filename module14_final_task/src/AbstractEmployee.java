public abstract class AbstractEmployee implements Employee {
    private static int count;                           //variable for creating a unique employee name
    protected String name;
    protected double monthSalary;
    protected final Company company;                    //the company that the employee belongs to

    //constructor that creates a ready-made employee with a salary.
    //different employees' salaries are formed in different ways
    public AbstractEmployee(Company company) {
        count++;
        this.name = getClass().getName() + count;
        this.company = company;
        setMonthSalary();
    }

    @Override
    public double getMonthSalary() {
        return monthSalary;
    }

    //different employees' salaries are formed in different ways
    public abstract void setMonthSalary();

    @Override
    public String toString() {
        return name + " " + getMonthSalary() + " руб.";
    }
}
