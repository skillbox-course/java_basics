public class TopManager extends AbstractEmployee{

    public TopManager(Company company) {
        super(company);
    }

    //salary is formed from baseSalary(from 60_000 to 100_000 (оклад)) and
    //bonus 150% of baseSalary if company income more than 10_000_000.
    @Override
    public void setMonthSalary() {
        double baseSalary = Math.round ((Math.random() * 40_000 + 60_000));

        this.monthSalary = (company.getIncome() > 10_000_000) ?
                baseSalary * 2.5 : baseSalary;
    }
}
