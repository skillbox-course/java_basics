
public class StartModule14_final_task {
    public static void main(String[] args) {

        Company company = new Company();
        company.generateOperators(180);
        company.generateManagers(80);
        company.generateTopManagers(10);
        System.out.println(company.getTopSalaryStaff(15));
        System.out.println(company.getLowestSalaryStaff(30));
        System.out.println(company.getStaff().size());
        company.firePercent(50);
        System.out.println("=======================================");
        System.out.println(company.getStaff().size());
        company.setIncome(11_000_000);
        System.out.println(company.getTopSalaryStaff(15));
        System.out.println(company.getLowestSalaryStaff(30));
    }
}
