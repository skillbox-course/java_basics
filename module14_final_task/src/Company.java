import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    private int income;
    private final List<Employee> staff = new ArrayList<>();                     //includes all employees
    private final List<Operator> operators = new ArrayList<>();
    private final List<Manager> managers = new ArrayList<>();
    private final List<TopManager> topManagers = new ArrayList<>();

    public void hire(Employee employee) {
        staff.add(employee);
    }

    public void hireAll(List<Employee> employees) {
        staff.addAll(employees);
    }

    //dismiss employee, if one exists
    public boolean fire(Employee employee) {
        if (staff.contains(employee)) {
            staff.remove(employee);
            return true;
        } else {
            System.out.println("no such employee was found");
            return false;
        }
    }

    //arbitrary dismissal of a certain percentage of all staff
    public void firePercent(int persent) {
        if (persent < 100 && persent > 0){
            int count = (int) ((persent / 100.00) * staff.size());              //number of employees
            Collections.shuffle(staff);
            for (int i = 0; i < count; i++) {
                staff.remove(i);
            }
        }
    }

    //get top salary staff by count. Checking the count
    public List<Employee> getTopSalaryStaff(int count) {
        if (count <= 0) {
            return null;
        }
        if (count > staff.size()) {
            count = staff.size();
            System.out.println("umber of employee of the company " + staff.size());
        }
        //sort staff by descending salary
        Collections.sort(staff, new EmployeeComparator());
        Collections.reverse(staff);
        //creating a new list of employees with the highest salaries
        List<Employee> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(staff.get(i));
        }
        return result;
    }

    //get the lowest salary staff by count. Checking the count
    public List<Employee> getLowestSalaryStaff(int count) {
        if (count <= 0) {
            return null;
        }
        if (count > staff.size()) {
            count = staff.size();
            System.out.println("umber of employee of the company " + staff.size());
        }
        //sort staff by ascending salary
        Collections.sort(staff, new EmployeeComparator());
        //creating a new list of employees with the lowest salaries
        List<Employee> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(staff.get(i));
        }
        return result;
    }

    public double getIncome() {
        return income;
    }

    //creating a certain numbers of operators and adding them to the staff
    public void generateOperators(int count) {
        for (int i = 0; i < count; i++) {
            Operator operator = new Operator(this);
            operators.add(operator);
            staff.add(operator);
        }
    }

    //creating a certain numbers of managers and adding them to the staff
    public void generateManagers(int count) {
        for (int i = 0; i < count; i++) {
            Manager manager = new Manager(this);
            managers.add(manager);
            staff.add(manager);
        }
    }

    //creating a certain numbers of top managers and adding them to the staff
    public void generateTopManagers(int count) {
        for (int i = 0; i < count; i++) {
            TopManager topManager = new TopManager(this);
            topManagers.add(topManager);
            staff.add(topManager);
        }
    }

    //salary formation for top managers taking into account the company's income
    //used in setIncome method
    public void generateTopManagersSalary() {
        for (TopManager topManager : topManagers) {
            topManager.setMonthSalary();
        }
    }

    //set income and generate top managers salaries
    public void setIncome(int income) {
        this.income = income;
        generateTopManagersSalary();
    }

    public List<Employee> getStaff() {
        return staff;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public List<TopManager> getTopManagers() {
        return topManagers;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("income=");
        result.append(income);
        for (Employee employee : staff) {
            result.append("\n");
            result.append(employee);
        }
        return result.toString();
    }
}
