public class Operator extends AbstractEmployee{

    public Operator(Company company) {
        super(company);
    }

    //operator receives just a baseSalary from 60_000 to 100_000 (оклад)
    @Override
    public void setMonthSalary() {
        this.monthSalary = Math.round ((Math.random() * 40_000 + 60_000));
    }
}
