import java.util.Comparator;

public class EmployeeComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Employee employee1 = (Employee) o1;
        Employee employee2 = (Employee) o2;
        return Double.compare(employee1.getMonthSalary(), employee2.getMonthSalary());
    }
}
