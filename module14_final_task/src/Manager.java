public class Manager extends AbstractEmployee{

    public Manager(Company company) {
        super(company);
    }

    //salary is formed from baseSalary(from 60_000 to 100_000 (оклад)) and 5% of the money earned for the company.
    //Money earned for the company is a random number from 115_000 to 140_000
    @Override
    public void setMonthSalary() {
        double baseSalary = Math.round ((Math.random() * 40_000 + 60_000));
        double companyIncomeFromManager = Math.round ((Math.random() * 25_000 + 115_000));
        this.monthSalary = baseSalary + companyIncomeFromManager * 0.05;
    }
}
