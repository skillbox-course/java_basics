public class StartPrimitivesProject {
    public static void main(String[] args) {
        Container container = new Container();
        container.addCount(5672);
        System.out.println(container.getCount());

        // TODO: ниже напишите код для выполнения задания:
        //  С помощью цикла и преобразования чисел в символы найдите все коды
        //  букв русского алфавита — заглавных и строчных, в том числе буквы Ё.
        for (int i = 'А'; i <= 'А' + 63; i++) {
            System.out.print(" "+i + "-" + (char) i);
        }
        System.out.print(" "+(int)'Ё' + "-" + 'Ё');
        System.out.print(" "+(int)'ё' + "-" + 'ё');

    }
}