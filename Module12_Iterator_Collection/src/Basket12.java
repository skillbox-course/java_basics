import java.util.ArrayList;
import java.util.Collections;

public class Basket12 {
    private final ArrayList<String> products;
    private static final int DISCOUNTED_PRODUCT_COUNT = 3;

    public Basket12() {
        products = new ArrayList<>();
    }

    public void initProducts(){
        products.add("Milk");
        products.add("Meat");
        products.add("Bread");
        products.add("Jam");
        products.add("Juice");
        products.add("Cookies");
        Collections.sort(products);
    }

    //random 3 products are discounted. They are derived from shuffle collection
    public ArrayList<String> discountedProducts(){
        ArrayList<String> discountedProducts = new ArrayList<>();
        Collections.shuffle(products);
        for (int i = 0; i < DISCOUNTED_PRODUCT_COUNT; i++){
            discountedProducts.add(products.get(i));
        }
        return discountedProducts;
    }

    //adding a new element under a specific index
    public boolean add(String product){
        int index = Collections.binarySearch(products,product);
        if (index >= 0) {
            return false;
        }
        products.add(-index-1, product);
        System.out.println(-index + " - " + product);
        return true;
    }
}
