import java.util.*;

public class StartModule12 {
    public static void main(String[] args) {
        //testing Collections methods
        Basket12 basket12 = new Basket12();
        basket12.initProducts();
        basket12.add("Jelly");
        System.out.println(basket12.discountedProducts());

        //initialization variables
        //basket<name, count>
        ConversionExperiments conversionExperiments = new ConversionExperiments();
        String[] colorsArray = {"BLACK", "WHITE", "RED", "GREEN", "BLUE"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(colorsArray));
        HashMap<String, Integer> basket = new HashMap<>();
        basket.put("Milk", 7);
        basket.put("Butter", 3);
        basket.put("Bread", 5);

        //iterator for arrayList collection
        Iterator<String> iterator = arrayList.iterator();
        while(iterator.hasNext()){
            String color = iterator.next();
            if (color.equals("RED")){
                iterator.remove();
            }
        }
        System.out.println(arrayList);

        //converting some collections to others
        System.out.println(conversionExperiments.conversionHashMapToTreeMap(basket, new ProductComparator()));
        ArrayList<String> colorsList = conversionExperiments.conversionArrayToList(colorsArray);
        colorsArray = conversionExperiments.conversionListToArray(colorsList);
        HashSet<String> hashSet = conversionExperiments.conversionListToHashSet(colorsList);
        //output to console
        System.out.println(Arrays.toString(colorsArray));
        System.out.println(hashSet);
    }
}
