import java.util.*;

public class ConversionExperiments {
    public ArrayList<String> conversionArrayToList(String[] testArray){
        ArrayList<String> result = new ArrayList<>(Arrays.asList(testArray));
        result.addAll(Arrays.asList(testArray));
        return result;
    }
    public String[] conversionListToArray(List<String> testList){
        String[] result = new String[testList.size()];
        for (int i = 0; i < result.length; i++){
            result[i] = testList.get(i);
        }
        result = testList.toArray(result);
        return result;
    }
    public HashSet<String> conversionListToHashSet(List<String> testList){
        return new HashSet<>(testList);
    }
    public TreeMap<String, Integer> conversionHashMapToTreeMap(HashMap<String, Integer> hashMap,
                                                               Comparator comparator){
        TreeMap<String, Integer> result = new TreeMap<>(comparator);
        result.putAll(hashMap);
        return result;
    }
}
