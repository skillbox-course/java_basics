import java.util.Objects;

//test class with hashcode & equals for TreeMap
public class Product11 {
    private String itemName;
    private int itemPrice;

    public Product11(String itemName, int itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product11 product11 = (Product11) o;
        return itemPrice == product11.itemPrice && Objects.equals(itemName, product11.itemName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemName, itemPrice);
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString() {
        return "Order11{" +
                "itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                '}';
    }
}
