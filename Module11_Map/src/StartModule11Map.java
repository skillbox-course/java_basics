import java.util.HashMap;

public class StartModule11Map {
    public static void main(String[] args) {

        // =====testing TreeMap=====
        TreeBasket treeBasket = new TreeBasket();
        treeBasket.add(new Product11("juice",100), 2);
        treeBasket.add(new Product11("muesli",150), 4);
        treeBasket.add(new Product11("milk",75), 3);
        treeBasket.add(new Product11("bread",50), 2);
        System.out.println(treeBasket);

        // =====testing HashMap=====
        HashBasket basket = new HashBasket();
        basket.add(new Product11("juice",100), 2);
        basket.add(new Product11("muesli",150), 4);
        basket.add(new Product11("juice",100), 3);
        System.out.println(basket);
        System.out.println(basket.anotherToString());

        HashMap<String, Product11> testBasket = new HashMap<>();
        testBasket.put("123",new Product11("milk",70));
        testBasket.put("234",new Product11("bread",50));
        testBasket.put("345",new Product11("meat",200));
        testBasket.put("345",new Product11("cheese",150));
        System.out.println(testBasket.get("345"));
    }
}