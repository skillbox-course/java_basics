import java.util.Map;
import java.util.TreeMap;

//class for TreeSet collection by products11, analog HashBasket, but use TreeMap
public class TreeBasket {
    private TreeMap<Product11, Integer> products;

    public TreeBasket() {
        this.products = new TreeMap<>(new Product11Comparator());
    }
    public void add(Product11 product, int count){
        if (!products.containsKey(product)) {
            products.put(product, 0);
        }
        //key = product, count = value. Get value(count) by key(product)
        products.put(product, products.get(product) + count);
    }
    public void add(Product11 product){
        add(product, 1);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Product11, Integer> productEntry : products.entrySet()){
            result.append(productEntry.getKey());
            result.append(" - ");
            result.append(productEntry.getValue());
            result.append("\n");
        }
        return result.toString();
    }
}
