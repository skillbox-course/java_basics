import java.util.HashMap;
import java.util.Map;

//class for products HashSet
public class HashBasket {
    //Map of product and count
    private HashMap<Product11, Integer> products;

    public HashBasket() {
        this.products = new HashMap<>();
    }
    public void add(Product11 product, int count){
        if (!products.containsKey(product)) {
            products.put(product, 0);
        }
        //key = product, count = value. Get value(count) by key(product)
        products.put(product, products.get(product) + count);
    }
    public void add(Product11 product){
        add(product, 1);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Product11 product : products.keySet()){
            result.append(product);
            result.append(" - ");
            result.append(products.get(product));
            result.append("\n");
        }
        return result.toString();
    }
    public String anotherToString(){
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Product11, Integer> entry : products.entrySet()){
            result.append(entry.getKey());
            result.append(" - ");
            result.append(entry.getValue());
            result.append("\n");
        }
        return result.toString();
    }
}
