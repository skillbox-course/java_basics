import java.util.Comparator;

public class Product11Comparator implements Comparator<Product11> {
    //compare by product price
    @Override
    public int compare(Product11 p1, Product11 p2) {
        return -Integer.compare(p1.getItemPrice(), p2.getItemPrice());
    }
}
