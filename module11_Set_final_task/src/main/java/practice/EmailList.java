package practice;

import java.util.List;
import java.util.TreeSet;

public class EmailList {
    private TreeSet<String> emailList = new TreeSet<>();

    //adding and checking a new email
    public void add(String email) {
        // TODO: валидный формат email добавляется, email это строка, она быть может любой
        // принять решение добавлять аргумент email или нет должен этот метод
        String emailRegex = "[\\w|.]{1,64}@[\\w]{1,263}[.][\\w]{2,3}";
        if (email.toLowerCase().matches(emailRegex)){
            emailList.add(email.toLowerCase());
        } else {
            System.out.println("Неверный формат email");
        }
    }

    public List<String> getSortedEmails() {
        // TODO: возвращается сортированный список электронных адресов в алфавитном порядке
        return List.copyOf(emailList);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String email : emailList){
            result.append(email);
            result.append("\n");
        }
        return result.toString().trim();
    }
}
