import practice.EmailList;

import java.util.Scanner;

public class Main {
    public static final String WRONG_EMAIL_ANSWER = "Неверный формат email";
    
    /* TODO:
        Пример вывода списка Email, после ввода команды LIST в консоль:
        test@test.com
        hello@mail.ru
        - каждый адрес с новой строки
        - список должен быть отсортирован по алфавиту
        - email в разных регистрах считается одинаковыми
           hello@skillbox.ru == HeLLO@SKILLbox.RU
        - вывод на печать должен быть в нижнем регистре
           hello@skillbox.ru
        Пример вывода сообщения об ошибке при неверном формате Email:
        "Неверный формат email"
    */

    public static void main(String[] args) {
        //initialization of required variables
        EmailList emailList = new EmailList();
        Scanner scanner = new Scanner(System.in);
        String[] inputWords;
        //reading a command from console and parse it using switch - case operator
        while (true) {
            System.out.println("\nВведите команду (для выхода наберите \"0\"):");
            String input = scanner.nextLine();
            if (input.equals("0")) {
                break;
            }
            inputWords = input.split("\s");
            
            //TODO: write code here
            switch (inputWords[0].toUpperCase()){
                case "ADD":
                    emailList.add(inputWords[1]);
                    break;
                case "LIST":
                    System.out.println(emailList);
                    break;
            }
        }
    }
}
