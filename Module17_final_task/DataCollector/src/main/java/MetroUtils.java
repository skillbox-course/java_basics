import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MetroUtils {
    private final List<File> csvFiles = new ArrayList<>();
    private final List<File> jsonFiles = new ArrayList<>();

    public static final String PATH_TO_WEB_PAGE = "https://skillbox-java.github.io/";
    public static final String PATH_TO_DATA_DIR = "src/main/resources/data";
    private static final String PATH_TO_STATIONS_JSON = "src/main/resources/metro/stations.json";
    private static final String PATH_TO_METRO_JSON = "src/main/resources/metro/metro.json";

    // recursively go through the directory and look for files with the extension csv
    public List<File> getCSVFilesFromDir(String path){
        File file = new File(path);
        if (file.isFile() && file.getName().contains(".csv")){
            csvFiles.add(file);
        }
        if (file.isDirectory()){
            File[] files = file.listFiles();
            for(File f : files){
                getCSVFilesFromDir(f.getPath());
            }
        }
        return csvFiles;
    }

    // recursively go through the directory and look for files with the extension json
    public List<File> getJsonFilesFromDir(String path) {
        File file = new File(path);
        if (file.isFile() && file.getName().contains(".json")) {
            jsonFiles.add(file);
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                getJsonFilesFromDir(f.getPath());
            }
        }
        return jsonFiles;
    }

    // parse csv file to string lines.
    // Skip first line(column names). Parse each line to stationName and dateOfFoundation
    // create new stations from strings and add them to result list of stations
    public List<Station> parseCSVFileToStations(File csvFile){
        List<Station> stations = new ArrayList<>();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        try {
            List<String> lines = Files.readAllLines(Paths.get(csvFile.getPath()));
            String firstLine = null;
            for (String line : lines){
                if (firstLine == null){
                    firstLine = line;
                    continue;
                }
                String[] columns = line.split(",");
                if (columns.length == 2){
                    Station station = new Station();
                    station.setName(columns[0].toUpperCase());
                    station.setDateOfFoundation(LocalDate.parse(columns[1], dateFormatter));
                    stations.add(station);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return stations;
    }

    // parse json file to JsonNode with stations
    // parse each station from stations to java object and add it to result list
    public List<Station> parseJsonFileToStations(File jsonFile){
        List<Station> resultStations = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonFileAsString = Files.readString(Paths.get(jsonFile.getPath()));
            JsonNode stationsNode = objectMapper.readTree(jsonFileAsString);
            for (JsonNode stationNode : stationsNode){
                Station station = new Station();
                station.setName(stationNode.get("station_name").asText().toUpperCase());
                station.setDepth(stationNode.get("depth").asText());
                resultStations.add(station);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return resultStations;
    }

    // Getting htmlDoc from web page. Getting line number from tag div class = js-metro-stations
    // Getting every htmlStation from line(div class = js-metro-stations)
    // Parsing elementStation to objectStation and adding it to result list
    public List<Station> getStationsFromWebLink(String link){
        List<Station> resultStations = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(link).get();
            Elements metroLines = doc.select("div.js-metro-stations");

            for (Element elementLine : metroLines){
                Document htmlStationsOfLine = Jsoup.parse(elementLine.html());
                Elements stationsOfLine = htmlStationsOfLine.select("p.single-station");

                for (Element elementStation : stationsOfLine){
                    Station station = new Station();
                    station.setName(elementStation.getElementsByClass("name").text().toUpperCase());
                    station.setLineNumber(elementLine.attr("data-line"));
                    boolean hasConnection =
                            elementStation.getElementsByClass("t-icon-metroln").hasAttr("title");
                    station.setHasConnection(hasConnection);
                    resultStations.add(station);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return resultStations;
    }

    // getting lists of stations from webPage, csvFiles, jsonFiles
    // and adding them to StationsWrapper object
    public StationsWrapper getAllStations(){
        StationsWrapper stationsWrapper =
                new StationsWrapper(getStationsFromWebLink(PATH_TO_WEB_PAGE));
        getCSVFilesFromDir(PATH_TO_DATA_DIR)
                .forEach(csvFile -> stationsWrapper.addStations(parseCSVFileToStations(csvFile)));
        getJsonFilesFromDir(PATH_TO_DATA_DIR)
                .forEach(jsonFile -> stationsWrapper.addStations(parseJsonFileToStations(jsonFile)));
        return stationsWrapper;
    }

    // Getting htmlDoc from web page. Getting htmlLine from <span class="js-metro-line
    // Parsing htmlLine to object lines
    public List<Line> getLinesFromWebLink(String link){
        List<Line> lines = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(link).get();
            Elements metroLines = doc.select("span.js-metro-line");
            for (Element element : metroLines){
                Line line = new Line();
                line.setName(element.text());
                line.setNumber(element.attr("data-line"));
                lines.add(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    // using ObjectMapper convert StationsWrapper(from getAllStations method) to json object
    // station property with null value not serialized
    public void writeStationsToJsonFile(StationsWrapper stationsWrapper){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        try {
            String stationAsJsonString = objectMapper.writeValueAsString(stationsWrapper);
            FileWriter fileWriter = new FileWriter(PATH_TO_STATIONS_JSON);
            fileWriter.write(stationAsJsonString);
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // parse metro object to json file using ObjectMapper
    public void writeMetroToJsonFile(Metro metro){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String metroAsJsonString = objectMapper.writeValueAsString(metro);
            FileWriter fileWriter = new FileWriter(PATH_TO_METRO_JSON);
            fileWriter.write(metroAsJsonString);
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}