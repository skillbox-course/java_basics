import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// class - decorator for list of stations
@Data
public class StationsWrapper {
    private List<Station> stations;

    public StationsWrapper(List<Station> stations) {
        this.stations = stations;
    }

    // adding a station. If station already exists in stations list,
    // only new properties are added
    public void addStation(Station station){
        if (getStationByName(station.getName()) == null){
            stations.add(station);
        }
        Station currentStation = getStationByName(station.getName());
        if (currentStation.getDepth() == null){
            currentStation.setDepth(station.getDepth());
        }
        if (currentStation.getDateOfFoundation() == null){
            currentStation.setDateOfFoundation(station.getDateOfFoundation());
        }
    }

    // adding list of stations using addStation method
    public void addStations(List<Station> stations){
        stations.forEach(this::addStation);
    }

    // getting station by name from list stations
    public Station getStationByName(String stationName){
        for (Station station : stations) {
            if (station.getName().equals(stationName)) {
                return station;
            }
        }
        return null;
    }

    @JsonIgnore
    public List<Station> getStationsWithLineNumber(){
        return stations.stream()
                .filter(station -> station.getLineNumber() != null)
                .collect(Collectors.toList());
    }

    public List<Station> getStationsByLineNumber(String lineNumber){
        return getStationsWithLineNumber().stream()
                .filter(station -> station.getLineNumber().equals(lineNumber))
                .collect(Collectors.toList());
    }

    @JsonIgnore
    public List<String> getStationsNames(){
        return stations.stream()
                .map(Station::getName)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    public Map<String, List<Station>> getStationsGroupingByLineNumber(){
        return getStationsWithLineNumber().stream()
                .collect(Collectors.groupingBy(Station::getLineNumber));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        stations.forEach(station -> result.append(station.toString()).append("\n"));
        return result.toString();
    }
}
