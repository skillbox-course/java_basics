import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDate;

// pojo java object. properties with null value are not serialized to json
@Data
public class Station {
    private String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String lineNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    private LocalDate dateOfFoundation;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String depth;
    private boolean hasConnection;

    @Override
    public String toString() {
        return "name: " + name +
                " - line:" + lineNumber +
                " - has connection: " + hasConnection +
                " - " + dateOfFoundation +
                " - depth: " + depth;
    }
}
