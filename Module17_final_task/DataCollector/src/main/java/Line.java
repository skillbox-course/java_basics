import lombok.Data;

@Data
public class Line {
    private String number;
    private String name;

    @Override
    public String toString() {
        return number + " - " + name;
    }
}
