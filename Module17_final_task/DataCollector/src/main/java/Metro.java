import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Metro {
    private List<Line> lines = new ArrayList<>();
    private Map<String, List<String>> stations = new HashMap<>();   // Map<lineNumber, list of stationsName>
    @JsonIgnore
    private Map<String, List<Station>> stationsMap = new HashMap<>();

    public static final String PATH_TO_WEB_PAGE = "https://skillbox-java.github.io/";

    public Metro(StationsWrapper stationsWrapper) {
        createLines();
        createStations(stationsWrapper);
    }

    private void createLines() {
        MetroUtils metroUtils = new MetroUtils();
        lines.addAll(metroUtils.getLinesFromWebLink(PATH_TO_WEB_PAGE));
    }

    // create stations, grouping by lineNumber, from files using metroUtils class
    private void createStations(StationsWrapper stationsWrapper) {
        for (Line line : lines){
            List<Station> lineStations =
                    stationsWrapper.getStationsByLineNumber(line.getNumber());
            stationsMap.put(line.getNumber(), lineStations);
            List<String> stationsName = new StationsWrapper(lineStations).getStationsNames();
            stations.put(line.getNumber(), stationsName);
        }
    }


}
