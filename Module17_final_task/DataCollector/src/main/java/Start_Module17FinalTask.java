
public class Start_Module17FinalTask {
    public static void main(String[] args) {

        MetroUtils metroUtils = new MetroUtils();
        StationsWrapper allStations = metroUtils.getAllStations();
        Metro metro = new Metro(allStations);

        metroUtils.writeMetroToJsonFile(metro);
        metroUtils.writeStationsToJsonFile(allStations);
    }
}
