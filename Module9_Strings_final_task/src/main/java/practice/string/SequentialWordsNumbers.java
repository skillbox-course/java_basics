package practice.string;

public class SequentialWordsNumbers {

    public static String sequentialWordsNumbers(String text){
        // It's necessary to number the words in text without regex
        if (text.isBlank()) return "";
        //initialization variables
        StringBuilder result = new StringBuilder();
        int wordNumber = 0;
        int start = 0;
        int end = text.indexOf("\s",start);
        //loop for passing through text
        for (int i = 0; i < text.length(); i++){
            //If no left spaces, so it's last word. Adding its number and exit from loop
            if (end == -1) {
                result.append("(").append(++wordNumber).append(") ")
                        .append(text.substring(start));
                break;
            } else {
                //adding number of word and assigning values to variables for new iteration
                result.append("(").append(++wordNumber).append(") ")
                        .append(text.substring(start, end)).append(" ");
                i = end;
                start = end + 1;
                end = text.indexOf("\s",start);
            }
        }
        return result.toString();
    }
}
