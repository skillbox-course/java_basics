package practice.regex;

import java.util.Scanner;

public class PhoneCleanerRegex {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    while (true) {
      String input = scanner.nextLine();
      if (input.equals("0")) {
        scanner.close();
        break;
      }

      // TODO:напишите ваш код тут, результат вывести в консоль.
      //the phone number is entering into console.
      //its necessary to check the correctness of the phone number
      //and bring it to the standard format 71234567890
      String result ;
      //deleting correct symbols
      String regexIncorrectSymbols = "[+\\s()-]+";
      result = input.replaceAll(regexIncorrectSymbols,"");
      //adding 7 if number is ten - digit and changing first character from 8 to 7
      if (result.length() == 10) result = "7" + result;
      if (result.charAt(0) == '8') result = result.replaceFirst("8", "7");
      //regex of correct number
      String regexCorrectNumber = "[7]{1}[0-9]{10}";
      //output the result
      System.out.println(result.matches(regexCorrectNumber) ? result : "Неверный формат номера");
    }
  }

}
