package practice.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SplitText {

  public static void main(String[] args) {

  }

  public static String splitTextIntoWords(String text) {
    //TODO реализуйте метод
    //the text is passed into the method.
    // It's necessary to output just words each of one is on a new line
    // shouldn't use array
    if (text.isBlank()) return "";
    StringBuilder result = new StringBuilder();
    String regex = "[a-zA-Z]+";                       //regex for word
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(text);
    while (matcher.find()){
      result.append("\n").append(matcher.group());            //output each word on a new line
    }
    return result.substring(1,result.length());       //output result without first newline
  }

}