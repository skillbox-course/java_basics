import exceptions.CommandException;
import exceptions.EmailException;
import exceptions.IncorrectNumberOfComponentsInCommand;
import exceptions.PhoneException;

import java.util.HashMap;
import java.util.Map;

public class CustomerStorage {
    private final Map<String, Customer> storage;

    public CustomerStorage() {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws CommandException {
        final int INDEX_NAME = 0;
        final int INDEX_SURNAME = 1;
        final int INDEX_EMAIL = 2;
        final int INDEX_PHONE = 3;

        String[] components = data.split("\\s+");
        if (components.length != 4){
            throw new IncorrectNumberOfComponentsInCommand();
        }
        if (!components[INDEX_PHONE].matches("\\+[7]{1}[0-9]{10}")){
            throw new PhoneException();
        }
        if (!components[INDEX_EMAIL].matches("\\w[\\w|._-]+@[\\w]{2,15}\\.[a-zA-Z]{2,3}")){
            throw new EmailException();
        }
        String name = components[INDEX_NAME] + " " + components[INDEX_SURNAME];
        storage.put(name, new Customer(name, components[INDEX_PHONE], components[INDEX_EMAIL]));
    }

    public void listCustomers() {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name) {
        storage.remove(name);
    }

    public Customer getCustomer(String name) {
        return storage.get(name);
    }

    public int getCount() {
        return storage.size();
    }
}