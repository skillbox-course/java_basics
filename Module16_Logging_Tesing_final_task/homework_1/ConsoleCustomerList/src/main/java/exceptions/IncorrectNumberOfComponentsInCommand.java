package exceptions;

public class IncorrectNumberOfComponentsInCommand extends CommandException {
    public IncorrectNumberOfComponentsInCommand() {
        super("incorrect number of words in \"add\" command\nCorrect add command:\n"
                + "add Василий Петров vasily.petrov@gmail.com +79215637722");
    }
}
