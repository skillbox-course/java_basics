package exceptions;

public class EmailException extends CommandException {
    public EmailException() {
        super("Wrong email Exception "
                + "wrong email format\nCorrect email format: vasily.petrov@gmail.com");
    }
}
