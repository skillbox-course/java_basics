package exceptions;

public class CommandException extends Exception {
    public CommandException() {
        super("Wrong Command Exception");
    }

    public CommandException(String message) {
        super(message);
    }
}
