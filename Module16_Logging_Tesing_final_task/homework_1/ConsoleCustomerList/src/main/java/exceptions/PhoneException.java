package exceptions;

public class PhoneException extends CommandException {
    public PhoneException() {
        super("Wrong Phone Exception "
            + "wrong phone number format\nCorrect phone number format: +79121234567");
    }
}
