import exceptions.CommandException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

//TODO:Напишите в классе CustomerStorage проекта ConsoleCustomerList все варианты защиты от некорректных данных,
// которые вам удастся придумать и обнаружить. Создайте собственные классы исключений, которые должны выбрасываться:
//      при некорректном количестве компонентов в переданной строке с данными;
//      при неверном формате номера телефона;
//      при неправильном формате e-mail.
//TODO: Сделайте два отдельных лога в папке logs проекта с помощью log4j2
// и настройте конфигурацию log4j следующим образом:
//      logs/queries.log — заполняется информацией обо всех запросах к приложению;
//      logs/errors.log — заполняется информацией обо всех ошибках (возникших исключениях со всеми деталями).


public class Main {
    private static final String ADD_COMMAND = "add Василий Петров " +
            "vasily.petrov@gmail.com +79215637722";
    private static final String COMMAND_EXAMPLES = "\t" + ADD_COMMAND + "\n" +
            "\tlist\n\tcount\n\tremove Василий Петров";
    private static final String COMMAND_ERROR = "Wrong command! Available command examples: \n" +
            COMMAND_EXAMPLES;
    private static final String helpText = "Command examples:\n" + COMMAND_EXAMPLES;
    private static final Logger rootLogger = LogManager.getLogger(Main.class);
    private static final Logger warnLogger = LogManager.getLogger("warnLogger");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CustomerStorage executor = new CustomerStorage();

        while (true) {

            if (tokens[0].equals("add")) {
                try {
                    executor.addCustomer(tokens[1]);
                    rootLogger.log(Level.INFO, "user entered the command: ".concat(command));
                } catch (CommandException e){
                    warnLogger.log(Level.WARN, e.getMessage());
                } catch (Exception e){
                    warnLogger.log(Level.ERROR, e.getMessage());
                }
            } else if (tokens[0].equals("list")) {
                executor.listCustomers();
                rootLogger.log(Level.INFO, "user entered the command: ".concat(command));
            } else if (tokens[0].equals("remove")) {
                executor.removeCustomer(tokens[1]);
                rootLogger.log(Level.INFO, "user entered the command: ".concat(command));
            } else if (tokens[0].equals("count")) {
                System.out.println("There are " + executor.getCount() + " customers");
                rootLogger.log(Level.INFO, "user entered the command: ".concat(command));
            } else if (tokens[0].equals("help")) {
                System.out.println(helpText);
                rootLogger.log(Level.INFO, "user entered the command: ".concat(command));
            } else {
                warnLogger.log(Level.WARN, command);
                System.out.println(COMMAND_ERROR);
            }
        }
    }
}
