import core.Line;
import core.Station;
import junit.framework.TestCase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestRouteCalculator extends TestCase {

    RouteCalculator routeCalculator;
    List<Station> testRoute;
    Line line1, line2, line3;
    Station station1A, station1B, station1C, station1D;
    Station station2A, station2B, station2C, station2D;
    Station station3A, station3B, station3C, station3D;

    @Override
    public void setUp() throws Exception {
        StationIndex stationIndex = new StationIndex();
        routeCalculator = new RouteCalculator(stationIndex);
        testRoute = new ArrayList<>();

        line1 = new Line(1, "line1");
        line2 = new Line(2, "line2");
        line3 = new Line(3, "line3");

        station1A = new Station("Station1A", line1);
        station1B = new Station("Station1B", line1);
        station1C = new Station("Station1C", line1);
        station1D = new Station("Station1D", line1);
        station2A = new Station("Station2A", line2);
        station2B = new Station("Station2B", line2);
        station2C = new Station("Station2C", line2);
        station2D = new Station("Station2D", line2);
        station3A = new Station("Station3A", line3);
        station3B = new Station("Station3B", line3);
        station3C = new Station("Station3C", line3);
        station3D = new Station("Station3D", line3);

        line1.addStation(station1A);
        line1.addStation(station1B);
        line1.addStation(station1C);
        line1.addStation(station1D);
        line2.addStation(station2A);
        line2.addStation(station2B);
        line2.addStation(station2C);
        line2.addStation(station2D);
        line3.addStation(station3A);
        line3.addStation(station3B);
        line3.addStation(station3C);
        line3.addStation(station3D);

        stationIndex.addLine(line1);
        stationIndex.addLine(line2);
        stationIndex.addLine(line3);

        stationIndex.addConnection(Arrays.asList(station1B, station2B));
//        stationIndex.addConnection(Arrays.asList(station1C, station3C));
        stationIndex.addConnection(Arrays.asList(station2C, station3C));

        stationIndex.addAllStations(line1.getStations());
        stationIndex.addAllStations(line2.getStations());
        stationIndex.addAllStations(line3.getStations());
    }

    // testing RouteOnTheLine
    public void testGetShortestRoute1(){
        testRoute = routeCalculator.getShortestRoute(station1A, station1D);
        List<Station> expected = Arrays.asList(station1A, station1B, station1C,station1D);
        assertEquals(expected, testRoute);
    }

    // testing RouteWithOneConnection
    public void testGetShortestRoute2(){
        testRoute = routeCalculator.getShortestRoute(station1A, station2D);
        List<Station> expected = Arrays.asList(station1A, station1B, station2B, station2C,station2D);
        assertEquals(expected, testRoute);
    }

    // testing RouteWithTwoConnection
    public void testGetShortestRoute3(){
        testRoute = routeCalculator.getShortestRoute(station1A, station3D);
        List<Station> expected = Arrays.asList(station1A, station1B, station2B, station2C,station3C, station3D);
        assertEquals(expected, testRoute);
    }

    // calculate RouteWithOneConnection duration
    public void testCalculateDuration(){
        testRoute = Arrays.asList(station1A, station1B, station2B, station2C);
        double actual = RouteCalculator.calculateDuration(testRoute);
        double expected = 8.5;
        assertEquals(expected, actual);
    }

    // testing correct params RouteOnTheLine
    public void testGetRouteOnTheLine(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteOnTheLine", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1D, station1A);
            List<Station> routeOnTheLine = List.of(station1D, station1C, station1B, station1A);
            assertEquals(routeOnTheLine, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    // testing wrong params RouteWithTwoConnections. expected null
    public void testGetRouteOnTheLineWrongParamsCheckingForNull(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteOnTheLine", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1A, station3D);
            testRoute = null;
            assertEquals(testRoute, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    // testing correct params RouteWithOneConnection
    public void testGetRouteWithOneConnection(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteWithOneConnection", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1A, station2D);
            List<Station> routeOnTheLine = List.of(station1A, station1B, station2B, station2C, station2D);
            assertEquals(routeOnTheLine, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    // testing wrong params RouteWithTwoConnection. expected null
    public void testGetRouteWithOneConnectionWrongParamsCheckingForNull(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteWithOneConnection", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1A, station3D);
            testRoute = null;
            assertEquals(testRoute, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    // testing correct params RouteWithTwoConnections
    public void testGetRouteWithTwoConnections(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteWithTwoConnections", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1A, station3D);
            testRoute =
                    List.of(station1A, station1B, station2B, station2C, station3C, station3D);
            assertEquals(testRoute, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    // testing wrong params RouteWithOneConnections. expected null
    public void testGetRouteWithTwoConnectionsWrongParamsCheckingForNull(){
        try{
            Method method = RouteCalculator.class
                    .getDeclaredMethod("getRouteWithTwoConnections", Station.class, Station.class);
            method.setAccessible(true);
            List<Station> actual = (List<Station>) method.invoke(routeCalculator, station1A, station2D);
            testRoute = null;
            assertEquals(testRoute, actual);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void tearDown() throws Exception {

    }
}
