import core.Line;
import core.Station;

import java.util.*;
import java.util.stream.Collectors;

//почему называется StationIndex - ?????? что за index - ??????
public class StationIndex {
    // список линий (ключ - номер линии; значение - линия)
    private final Map<Integer, Line> number2line;
    // just all stations
    private final TreeSet<Station> stations;
    // connections (переход) - набор станций присоединенных к станции-ключу
    // переход сенная-садовая-спасская это три connections (connection1 <сенная, лист: садовая-спасская)
    // (connection2 <садовая, лист: сенная-спасская) (connection3 <спасская, лист: садовая-сенная)
    private final Map<Station, TreeSet<Station>> connections;

    public StationIndex() {
        number2line = new HashMap<>();
        stations = new TreeSet<>();
        connections = new TreeMap<>();
    }

    public void addStation(Station station) {
        stations.add(station);
    }

    public void addAllStations(Collection<Station> stations){
        stations.addAll(stations);
    }

    public void addLine(Line line) {
        number2line.put(line.getNumber(), line);
    }

    //поиск в списке переходов(connections) каждой станции из списка stations
    //если не найдено перехода с ключ-станцией из списка, то создается новый переход с этой станцией
    //логика добавления связанных станций к ключ-станции - ????????????????????
    //добавляются все станции из списка переданных в метод, проверок нет, метод публичный
    public void addConnection(List<Station> stations) {
        for (Station station : stations) {
            if (!connections.containsKey(station)) {
                connections.put(station, new TreeSet<>());
            }
            TreeSet<Station> connectedStations = connections.get(station);
            connectedStations.addAll(stations.stream()
                    .filter(s -> !s.equals(station)).collect(Collectors.toList()));
        }
    }

    public Line getLine(int number) {
        return number2line.get(number);
    }

    public Station getStation(String name) {
        for (Station station : stations) {
            if (station.getName().equalsIgnoreCase(name)) {
                return station;
            }
        }
        return null;
    }

    public Station getStation(String name, int lineNumber) {
        Station query = new Station(name, getLine(lineNumber));
        Station station = stations.ceiling(query);
        return station.equals(query) ? station : null;
    }

    public Set<Station> getConnectedStations(Station station) {
        return connections.containsKey(station) ?
                connections.get(station) : new TreeSet<>();
    }
}
