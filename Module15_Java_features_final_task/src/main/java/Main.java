import com.skillbox.airport.Airport;
import com.skillbox.airport.Flight;
import com.skillbox.airport.Terminal;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Airport airport = Airport.getInstance();
        findPlanesLeavingInTheNextTwoHours(airport)
                .forEach(System.out::println);
    }

    public static List<Flight> findPlanesLeavingInTheNextTwoHours(Airport airport) {
        //TODO Метод должден вернуть список рейсов вылетающих в ближайшие два часа.
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 2);

        return airport.getTerminals().stream()                      //getting all airport terminals
                .map(Terminal::getFlights)                          //getting list of every terminal flights lists
//                .map((Terminal t) -> t.getFlights())              //getting list of every terminal flights lists
                .reduce((commmonFlightsList, everyTerminalFlightsList) -> {
                    commmonFlightsList.addAll(everyTerminalFlightsList);
                    return commmonFlightsList;                             //combining all flights into one common list
                })
                .orElse(new ArrayList<>())                                  //getting allFlights list
                .stream()
                .filter((Flight f) -> calendar.getTime().after(f.getDate()))
                .filter((Flight f) -> f.getDate().after(new Date()))
                .filter((Flight f) -> f.getType().equals(Flight.Type.DEPARTURE))
//                .sorted(Comparator.comparing(Flight::getDate))
                .collect(Collectors.toList());
    }

}