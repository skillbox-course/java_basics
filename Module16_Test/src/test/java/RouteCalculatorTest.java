import core.Line;
import core.Station;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class RouteCalculatorTest extends TestCase {
    List<Station> route;
    // data initialization
    @Override
    public void setUp() throws Exception {
        route = new ArrayList<>();
        Line line1 = new Line(1, "line1");
        Line line2 = new Line(2, "line2");

        route.add(new Station("Station1A", line1));
        route.add(new Station("Station1B", line1));
        route.add(new Station("Station2A", line2));
        route.add(new Station("Station2B", line2));
    }

    public void testCalculateDuration() {
        double actual = RouteCalculator.calculateDuration(route);
        double expected = 8.5;
        assertEquals(expected, actual);
    }

    //deleting data
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
