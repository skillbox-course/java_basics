import java.util.ArrayList;

public class StartModule14 {
    public static void main(String[] args) {
        SmartHouse smartHouse = new SmartHouse(new ArrayList<>());
        System.out.println(smartHouse.getElectricDevices());
        
        Lamp lamp = new Lamp(100);
        lamp.setType(Lamp.Type.LED);
    }
}