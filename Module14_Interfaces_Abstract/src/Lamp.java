public class Lamp extends LightingDevice{
    public enum Type {
        INCANDESCENT, FILAMENT, LED, LUMINESCENT
    }

    private Type type;

    public Lamp(int power) {
        super(power);
    }

    @Override
    public double getEnergyConsumption(){
        return power * brightness;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}