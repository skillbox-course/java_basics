import java.util.ArrayList;

public class SmartHouse {
    private ArrayList<ElectricDevice> electricDevices;

    public SmartHouse(ArrayList<ElectricDevice> electricDevices) {
        this.electricDevices = electricDevices;
        initLightingDevices();
    }

    public void switchOffAllDevices() {
        for (ElectricDevice device : electricDevices) {
            device.switchOff();
        }
    }

    public double getAllEnergyConsumption() {
        double sum = 0;
        for (ElectricDevice device : electricDevices) {
            sum += device.getEnergyConsumption();
        }
        return sum;
    }

    public void switchOffAllLight(){
        for (ElectricDevice device : electricDevices){
            if (device instanceof ElectricDevice){
                device.switchOff();
            }
        }
    }

    private void initLightingDevices() {
        electricDevices.add(new Lamp(100));
        electricDevices.add(new Lamp(50));
        electricDevices.add(new Lamp(120));
        electricDevices.add(new Lamp(80));
        electricDevices.add(new Chandelier(100, 4));
        electricDevices.add(new Chandelier(250, 5));
        electricDevices.add(new Chandelier(120, 3));
    }

    public ArrayList<ElectricDevice> getElectricDevices() {
        return electricDevices;
    }
}
