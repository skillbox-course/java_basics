public abstract class LightingDevice
        implements ElectricDevice, Comparable {
    public static final double MAX_BRIGHTNESS = 1D;
    public static final double MIN_BRIGHTNESS = 0D;

    protected final int power;
    protected double brightness;

    public LightingDevice(int power) {
        this.power = power;
    }

    //passing level of brightness to method and check it for acceptable limits
    public void setBrightness(double level){
        if (level < MIN_BRIGHTNESS) {
            brightness = MIN_BRIGHTNESS;
        } else if (level > MAX_BRIGHTNESS) {
            brightness = MIN_BRIGHTNESS;
        } else {
            brightness = level;
        }
    }

    @Override
    public void switchOn() {
        setBrightness(MAX_BRIGHTNESS);
    }

    @Override
    public void switchOff() {
        setBrightness(MIN_BRIGHTNESS);
    }

    @Override
    public boolean isSwitchedOn() {
        return brightness > 0;
    }

    // 0.15 rate increases brightness by 15% and -0.15 reduces by 15%
    public void changeBrightness(double rate){
        double changed = brightness + brightness * rate;
        setBrightness(changed);
    }

    public double getBrightness() {
        return brightness;
    }

    @Override
    public int compareTo(Object o) {
        LightingDevice lightingDevice = (LightingDevice) o;
        return Integer.compare(power, lightingDevice.power);
    }

    @Override
    public String toString() {
        return "\n" + getClass().getName() + "{" +
                "power=" + power +
                ", brightness=" + brightness +
                "}";
    }
}